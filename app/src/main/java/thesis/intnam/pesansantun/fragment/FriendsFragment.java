package thesis.intnam.pesansantun.fragment;


import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import thesis.intnam.pesansantun.ChatActivity;
import thesis.intnam.pesansantun.R;
import thesis.intnam.pesansantun.Utils.ImageUtils;
import thesis.intnam.pesansantun.adapter.FriendListItem;
import thesis.intnam.pesansantun.data.PesanSantunDB;
import thesis.intnam.pesansantun.data.StaticConfig;
import thesis.intnam.pesansantun.entity.Friend;
import thesis.intnam.pesansantun.service.ServiceUtils;

import static thesis.intnam.pesansantun.data.StaticConfig.UID;
import static thesis.intnam.pesansantun.service.ServiceUtils.formatLongToCalendar;


public class FriendsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static int ACTION_START_CHAT = 1;
    private static FastItemAdapter<FriendListItem> fastItemAdapter;
    private static Context context;
    public FragFriendClickFloatButton onClickFloatButton;
    private RecyclerView recyclerListFrends;
    private CountDownTimer detectFriendOnline;
    private Boolean islistOnLongPressed;
    private ArrayList<FriendListItem> items;
    private LinearLayoutManager linearLayoutManager;

    public FriendsFragment() {
        onClickFloatButton = new FragFriendClickFloatButton();
    }

    public static void setFriendList(Friend friend) {

        Boolean isNotOnTheList = true;
        fastItemAdapter = fastItemAdapter == null ? new FastItemAdapter<FriendListItem>() : fastItemAdapter;

        for (int i = 0; i < fastItemAdapter.getAdapterItems().size(); i++) {
            Friend fr = fastItemAdapter.getAdapterItem(i).friend;

            if (fr.id.equals(friend.id)) {
                fr.name = friend.name;
                fr.avata = friend.avata;
                fastItemAdapter.notifyDataSetChanged();
                isNotOnTheList = false;
            }

        }
        if (isNotOnTheList) {
            fastItemAdapter.add(new FriendListItem(friend));
            fastItemAdapter.notifyDataSetChanged();
        }
        checkFriendStatus(friend);

    }

    private static void checkFriendStatus(final Friend friend) {

        final String fid = friend.id;
        FirebaseDatabase.getInstance().getReference().child("user/" + fid + "/status").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    HashMap mapStatus = (HashMap) dataSnapshot.getValue();

                    Calendar lastOnline = formatLongToCalendar((long) mapStatus.get("timestamp"));
                    lastOnline.set(Calendar.MINUTE, lastOnline.get(Calendar.MINUTE) + 1);
                    Calendar now = formatLongToCalendar(System.currentTimeMillis());

                    if (lastOnline.after(now)) {
                        friend.status.isOnline = true;
                    } else {
                        friend.status.isOnline = false;
                    }
                    friend.status.timestamp = (long) mapStatus.get("timestamp");

                    Boolean isOnTheList = false;
                    int position = 0;
                    for (int i = 0; i < fastItemAdapter.getAdapterItems().size(); i++) {
                        Friend fr = fastItemAdapter.getAdapterItem(i).friend;

                        if (fr.id.equals(friend.id)) {
                            fr.status.timestamp = friend.status.timestamp;
                            fastItemAdapter.notifyDataSetChanged();
                            isOnTheList = true;
                        }
                    }
                    if (!isOnTheList) {
                        fastItemAdapter.add(new FriendListItem(friend));
                    }
                    PesanSantunDB.getInstance(context).updateFriendStatusInfo(friend);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_people, container, false);
        recyclerListFrends = (RecyclerView) layout.findViewById(R.id.recycleListFriend);
        ArrayList<Friend> items = PesanSantunDB.getInstance(getContext()).getAllFriendList();
        for (Friend friend : items) {
            setFriendList(friend);
        }
        setDataFriendsList();
        updateOnlineStatus();
        islistOnLongPressed = false;
        context = layout.getContext();
        return layout;
    }

    private void updateOnlineStatus() {
        detectFriendOnline = new CountDownTimer(System.currentTimeMillis(), StaticConfig.TIME_TO_REFRESH) {
            @Override
            public void onTick(long l) {
                ServiceUtils.updateUserStatus(getContext());
            }

            @Override
            public void onFinish() {

            }
        };
        detectFriendOnline.start();
    }

    private void setDataFriendsList() {
        fastItemAdapter = new FastItemAdapter<>();
        items = new ArrayList<>();
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerListFrends.setLayoutManager(linearLayoutManager);

        items = PesanSantunDB.getInstance(getContext()).getAllFriendListItem();

        fastItemAdapter.add(items);
        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<FriendListItem>() {
            @Override
            public boolean onClick(View v, IAdapter<FriendListItem> adapter, final FriendListItem item, int position) {

                final AlertDialog alertDialog;

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setView(View.inflate(getContext(), R.layout.friends_profile, null));
                alertDialog = builder.create();
                alertDialog.setCancelable(true);
                alertDialog.show();

                TextView displayName = (TextView) alertDialog.findViewById(R.id.displayName);
                CircleImageView profile_picture = (CircleImageView) alertDialog.findViewById(R.id.profile_picture);
                LinearLayout chatButton = (LinearLayout) alertDialog.findViewById(R.id.chatButton);

                chatButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(getContext(), ChatActivity.class);

                        intent.putExtra(StaticConfig.INTENT_KEY_CHAT_FRIEND, item.friend.name);
                        ArrayList<CharSequence> idFriend = new ArrayList<CharSequence>();
                        idFriend.add(item.friend.id);
                        intent.putCharSequenceArrayListExtra(StaticConfig.INTENT_KEY_CHAT_ID, idFriend);
                        intent.putExtra(StaticConfig.INTENT_KEY_CHAT_ROOM_ID, item.friend.idRoom);
                        startActivityForResult(intent, FriendsFragment.ACTION_START_CHAT);
                    }
                });
                displayName.setText(item.friend.name);
                profile_picture = setImageAvatar(getContext(), item.friend.avata, profile_picture);

                return true;
            }
        });

        recyclerListFrends.setAdapter(fastItemAdapter);
    }


    private CircleImageView setImageAvatar(Context context, String avata, CircleImageView picture) {
        try {
            Resources res = Resources.getSystem();
            Bitmap src;
            if (avata.equals("default")) {
                src = BitmapFactory.decodeResource(res, R.drawable.default_avata);
            } else {
                byte[] decodedString = Base64.decode(avata, Base64.DEFAULT);
                src = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            }

            picture.setImageDrawable(ImageUtils.roundedImage(context, src));
            picture.setDrawingCacheEnabled(true);
        } catch (Exception e) {
        }
        return picture;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        detectFriendOnline.cancel();
    }


    @Override
    public void onRefresh() {
        detectFriendOnline.cancel();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        detectFriendOnline.cancel();
        if (ACTION_START_CHAT == requestCode && data != null) {
            String idFriend = data.getStringExtra("idFriend");
            ChatFragment.enableListenerByIdFriend(idFriend);
        }
    }

    private void getFriendInfo(final String id) {

        FirebaseDatabase.getInstance().getReference().child("user/" + id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    Friend friend = new Friend();
                    HashMap mapUserInfo = (HashMap) dataSnapshot.getValue();
                    friend.name = (String) mapUserInfo.get("name");
                    friend.email = (String) mapUserInfo.get("email");
                    friend.avata = (String) mapUserInfo.get("avata");
                    friend.id = id;
                    friend.idRoom = id.compareTo(StaticConfig.UID) > 0 ? (StaticConfig.UID + id).hashCode() + "" : "" + (id + StaticConfig.UID).hashCode();

                    PesanSantunDB.getInstance(context).saveFriendInfo(friend);

                    ChatFragment.updateChatList(friend);
                    FriendsFragment.setFriendList(friend);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public class FragFriendClickFloatButton implements View.OnClickListener {
        Context context;
        LovelyProgressDialog dialogWait;

        public FragFriendClickFloatButton() {
        }

        public FragFriendClickFloatButton getInstance(Context context) {
            this.context = context;
            dialogWait = new LovelyProgressDialog(context);
            return this;
        }

        @Override
        public void onClick(final View view) {

            final AlertDialog alertDialog;

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setView(getLayoutInflater().inflate(R.layout.dialog_add_friend, null));
            alertDialog = builder.create();
            alertDialog.setCancelable(false);
            alertDialog.show();


            Button cancelButton = (Button) alertDialog.findViewById(R.id.cancel);
            Button okButton = (Button) alertDialog.findViewById(R.id.ok);

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AutoCompleteTextView friendEmailTxt = (AutoCompleteTextView) alertDialog.findViewById(R.id.addFriend);
                    String friendEmail = friendEmailTxt.getText().toString();

                    if (friendEmail.isEmpty()) {
                        friendEmailTxt.setError(getString(R.string.email_cant_be_empty));
                    } else {
                        Pattern VALID_EMAIL_ADDRESS_REGEX =
                                Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
                        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(friendEmail);

                        if (!matcher.find()) {
                            friendEmailTxt.setError(getString(R.string.wrong_email_format));
                        } else {
                            Boolean isExist = PesanSantunDB.getInstance(getContext()).getFriendByEmail(friendEmail);

                            if (isExist) {
                                friendEmailTxt.setError(getString(R.string.email_already_exist_on_contact));
                            } else {
                                alertDialog.dismiss();
                                findIDEmail(friendEmail);
                            }

                        }
                    }
                }
            });


        }

        private void findIDEmail(String email) {
            dialogWait.setCancelable(true)
                    .setIcon(R.drawable.ic_add_friend)
                    .setTitle("Finding friend....")
                    .setTopColorRes(R.color.primary)
                    .show();
            FirebaseDatabase.getInstance().getReference().child("user").orderByChild("email").equalTo(email).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    dialogWait.dismiss();
                    if (dataSnapshot.getValue() == null) {
                        //email not found
                        new LovelyInfoDialog(context)
                                .setTopColorRes(R.color.primary)
                                .setIcon(R.drawable.ic_add_friend)
                                .setTitle("Failed")
                                .setMessage(getString(R.string.email_not_found))
                                .show();
                    } else {
                        String id = ((HashMap) dataSnapshot.getValue()).keySet().iterator().next().toString();
                        if (id.equals(UID)) {
                            new LovelyInfoDialog(context)
                                    .setTopColorRes(R.color.colorAccent)
                                    .setIcon(R.drawable.ic_add_friend)
                                    .setTitle("Fail")
                                    .setMessage("Email not valid")
                                    .show();
                        } else {
                            HashMap userMap = (HashMap) ((HashMap) dataSnapshot.getValue()).get(id);
                            Friend user = new Friend();
                            user.name = (String) userMap.get("name");
                            user.email = (String) userMap.get("email");
                            user.avata = (String) userMap.get("avata");
                            user.id = id;
                            user.idRoom = id.compareTo(UID) > 0 ? (UID + id).hashCode() + "" : "" + (id + UID).hashCode();
                            addFriendIntoFirebaseAndLocalDb(id, user);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        private void addFriendIntoFirebaseAndLocalDb(final String idFriend, Friend userInfo) {
            dialogWait.setCancelable(false)
                    .setIcon(R.drawable.ic_add_friend)
                    .setTitle("Add friend....")
                    .setTopColorRes(R.color.primary)
                    .show();

            addFriend(idFriend);
            PesanSantunDB.getInstance(getContext()).addFriend(userInfo);

        }

        /**
         * Add friend
         *
         * @param idFriend
         */
        private void addFriend(final String idFriend) {


            FirebaseDatabase.getInstance().getReference().child("friend/" + StaticConfig.UID).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    ArrayList<String> listFriendId = new ArrayList();
                    Boolean isExist = false;
                    if (dataSnapshot.getValue() != null) {
                        HashMap mapRecord = (HashMap) dataSnapshot.getValue();
                        Iterator listKey = mapRecord.keySet().iterator();

                        while (listKey.hasNext()) {
                            String key = listKey.next().toString();
                            String existingFriendId = mapRecord.get(key).toString();

                            if (existingFriendId.equals(idFriend))
                                isExist = true;
                        }
                    }
                    if (!isExist) {
                        includeFriendIntoMyContact(idFriend);
                        includeMeIntoFriendContact(idFriend);
                        getFriendInfo(idFriend);
                    }
                    dialogWait.dismiss();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        private void includeFriendIntoMyContact(String idFriend) {
            FirebaseDatabase.getInstance().getReference().child("friend/" + UID).push().setValue(idFriend)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            dialogWait.dismiss();
                            if (task.isSuccessful()) {
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            dialogWait.dismiss();
                            new LovelyInfoDialog(context)
                                    .setTopColorRes(R.color.colorAccent)
                                    .setIcon(R.drawable.ic_add_friend)
                                    .setTitle("False")
                                    .setMessage("False to add friend")
                                    .show();
                        }
                    });
        }

        private void includeMeIntoFriendContact(String idFriend) {
            FirebaseDatabase.getInstance().getReference().child("friend/" + idFriend).push().setValue(UID)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            dialogWait.dismiss();
                            if (task.isSuccessful()) {
                                new LovelyInfoDialog(context)
                                        .setTopColorRes(R.color.primary)
                                        .setIcon(R.drawable.ic_add_friend)
                                        .setTitle("Success")
                                        .setMessage("Add friend success")
                                        .show();
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            dialogWait.dismiss();
                            new LovelyInfoDialog(context)
                                    .setTopColorRes(R.color.colorAccent)
                                    .setIcon(R.drawable.ic_add_friend)
                                    .setTitle("False")
                                    .setMessage("False to add friend")
                                    .show();
                        }
                    });
        }

    }

}



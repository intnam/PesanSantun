package thesis.intnam.pesansantun.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import thesis.intnam.pesansantun.R;
import thesis.intnam.pesansantun.entity.Chat;

public class ChatItem extends AbstractItem<ChatItem, ChatItem.ViewHolder> {

    public Chat chat;

    public ChatItem(Chat chat) {
        this.chat = chat;
    }


    public ChatItem() {
        this.chat = new Chat();
    }
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }


    @Override
    public int getType() {
        return 0;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.chat_item;
    }

    @Override
    public void bindView(ViewHolder holder, List payloads) {
        super.bindView(holder, payloads);
        Context ctx = holder.itemView.getContext();
//        holder.key.setText(chat.getKey());
//        holder.value.setText(chat.getValue());

    }


    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

//
//        @BindView(R.id.key)
//        protected TextView key;
//
//        @BindView(R.id.value)
//        protected TextView value;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;

        }
    }
}

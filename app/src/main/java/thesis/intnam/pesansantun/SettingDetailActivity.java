package thesis.intnam.pesansantun;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import thesis.intnam.pesansantun.Utils.ImageUtils;
import thesis.intnam.pesansantun.data.PesanSantunDB;
import thesis.intnam.pesansantun.data.GroupDB;
import thesis.intnam.pesansantun.data.SharedPreferenceHelper;
import thesis.intnam.pesansantun.data.StaticConfig;
import thesis.intnam.pesansantun.entity.User;
import thesis.intnam.pesansantun.service.ServiceUtils;

public class SettingDetailActivity extends AppCompatActivity {

    private static final String TAG = "Profile-Settings";
    private static final int REQUEST_UPLOAD_PHOTO = 1;
    private static final int PICK_IMAGE = 21;
    private final Pattern ALFANUMERIC_ONLY_REGEX =
            Pattern.compile("^[a-zA-Z0-9_]*$", Pattern.CASE_INSENSITIVE);
    @BindView(R.id.profile_picture)
    protected CircleImageView profile_picture;
    @BindView(R.id.displayName)
    protected TextView displayName;
    @BindView(R.id.userEmail)
    protected TextView userEmail;
    private Context mContext;
    private Uri photoUrl;
    private StorageReference mStorageRef;
    private FirebaseAuth mAuth;
    private User myAccount;
    private FirebaseUser user;
    private LovelyProgressDialog waitingDialog;
    private SharedPreferenceHelper prefHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_detail);
        ButterKnife.bind(this);
        mContext = this;
        waitingDialog = new LovelyProgressDialog(mContext);
        getSupportActionBar().setTitle("Settings");
        mAuth = FirebaseAuth.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        user = mAuth.getCurrentUser();
        prefHelper = SharedPreferenceHelper.getInstance(mContext);
        myAccount = prefHelper.getUserInfo();

        if (myAccount != null) {
            displayName.setText(myAccount.name);
            userEmail.setText(myAccount.email);
            setImageAvatar(mContext, myAccount.avata);
        } else {
            retrieveDataFromFirebaseDB();
        }




    }

    private void retrieveDataFromFirebaseDB() {
        FirebaseDatabase.getInstance().getReference().child("user").child(StaticConfig.UID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                myAccount = dataSnapshot.getValue(User.class);
                if (myAccount != null) {
                    displayName.setText(myAccount.name);
                    setImageAvatar(mContext, myAccount.avata);
                    SharedPreferenceHelper preferenceHelper = SharedPreferenceHelper.getInstance(mContext);
                    preferenceHelper.saveUserInfo(myAccount);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @OnClick(R.id.profile_picture)
    public void changePhoto() {

        checkPermission();

    }

    private void takePicture() {

        Matisse.from(this)
                .choose(MimeType.of(MimeType.JPEG, MimeType.PNG))
                .countable(false)
//                .showSingleMediaType(true)
                .maxSelectable(1)
                .theme(R.style.Matisse_Dracula)
//                .addFilter(new GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
//                .gridExpectedSize(getResources().getDimensionPixelSize(R.dimen.grid_expected_size))
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                .thumbnailScale(0.85f)
                .imageEngine(new GlideEngine())
                .forResult(REQUEST_UPLOAD_PHOTO);
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(mContext,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(mContext,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {//Can add more as per requirement

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                    123);

        } else {
            takePicture();
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Integer.parseInt(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePicture();
            } else {
                Toast.makeText(mContext, "Need permission to read storage!", Toast.LENGTH_LONG).show();
            }
        }
    }

    @OnClick(R.id.change_userName)
    public void changeUserName() {
        final AlertDialog alertDialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(getLayoutInflater().inflate(R.layout.dialog_edit_username, null));
        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();


        Button cancelButton = (Button) alertDialog.findViewById(R.id.cancel);
        Button okButton = (Button) alertDialog.findViewById(R.id.ok);
        final AutoCompleteTextView newNameTextView = (AutoCompleteTextView) alertDialog.findViewById(R.id.edit_username);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String newName = newNameTextView.getText().toString();
                if (newName.isEmpty()) {
                    newNameTextView.setError(getResources().getString(R.string.name_cant_be_empty));
                } else {
                    Matcher matcher = ALFANUMERIC_ONLY_REGEX.matcher(newName.replaceAll(" ", ""));
                    if (matcher.find()) {
                        if (!myAccount.name.equals(newName)) {
                            FirebaseDatabase.getInstance().getReference().child("user").child(StaticConfig.UID)
                                    .child("name").setValue(newName).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        myAccount.name = newName;
                                        displayName.setText(myAccount.name);
                                        SharedPreferenceHelper prefHelper = SharedPreferenceHelper.getInstance(mContext);
                                        prefHelper.saveUserInfo(myAccount);
                                    }
                                }
                            });


                        }
                        alertDialog.dismiss();
                    } else {
                        newNameTextView.setError(getResources().getString(R.string.name_can_only_contain_alfanumeric));
                    }
                }

            }
        });


    }

    @OnClick(R.id.id_logout)
    public void doLogout() {
        FirebaseAuth.getInstance().signOut();
        PesanSantunDB.getInstance(mContext).dropDB();
        GroupDB.getInstance(mContext).dropDB();
        ServiceUtils.stopServiceFriendChat(mContext.getApplicationContext(), true);
        finish();
    }

    @OnClick(R.id.id_change_password)
    public void changePassword() {
        new AlertDialog.Builder(mContext)
                .setTitle("Password")
                .setMessage("Are you sure want to reset password?")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        resetPassword(myAccount.email);
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();

    }

    public void resetPassword(final String email) {
        mAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        new LovelyInfoDialog(mContext) {
                            @Override
                            public LovelyInfoDialog setConfirmButtonText(String text) {
                                findView(com.yarolegovich.lovelydialog.R.id.ld_btn_confirm).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dismiss();
                                    }
                                });
                                return super.setConfirmButtonText(text);
                            }
                        }
                                .setTopColorRes(R.color.primary)
                                .setIcon(R.drawable.ic_pass_reset)
                                .setTitle("Password Recovery")
                                .setMessage("Sent email to " + email)
                                .setConfirmButtonText("Ok")
                                .show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        new LovelyInfoDialog(mContext) {
                            @Override
                            public LovelyInfoDialog setConfirmButtonText(String text) {
                                findView(com.yarolegovich.lovelydialog.R.id.ld_btn_confirm).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dismiss();
                                    }
                                });
                                return super.setConfirmButtonText(text);
                            }
                        }
                                .setTopColorRes(R.color.primary)
                                .setIcon(R.drawable.ic_pass_reset)
                                .setTitle("False")
                                .setMessage("False to sent email to " + email)
                                .setConfirmButtonText("Ok")
                                .show();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_UPLOAD_PHOTO && resultCode == RESULT_OK) {
            Log.d("Matisse", "Uris: " + Matisse.obtainResult(data));
            CropImage.activity(Matisse.obtainResult(data).get(0))
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .setAspectRatio(1, 1)
                    .start(this);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                waitingDialog.setCancelable(false)
                        .setTitle("updating the profile....")
                        .setTopColorRes(R.color.primary)
                        .show();
                Uri file = result.getUri();
                try {

                    InputStream inputStream = mContext.getContentResolver().openInputStream(file);

                    Bitmap imgBitmap = BitmapFactory.decodeStream(inputStream);
                    imgBitmap = ImageUtils.cropToSquare(imgBitmap);
                    InputStream is = ImageUtils.convertBitmapToInputStream(imgBitmap);
                    final Bitmap liteImage = ImageUtils.makeImageLite(is,
                            imgBitmap.getWidth(), imgBitmap.getHeight(),
                            ImageUtils.AVATAR_WIDTH, ImageUtils.AVATAR_HEIGHT);

                    final String imageBase64 = ImageUtils.encodeBase64(liteImage);

                    FirebaseDatabase.getInstance().getReference().child("user").child(StaticConfig.UID)
                            .child("avata").setValue(imageBase64)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    waitingDialog.dismiss();
                                    if (task.isSuccessful()) {
                                        SharedPreferenceHelper preferenceHelper = SharedPreferenceHelper.getInstance(mContext);
                                        myAccount.avata = imageBase64;
                                        preferenceHelper.saveUserInfo(myAccount);
                                        profile_picture.setImageDrawable(ImageUtils.roundedImage(mContext, liteImage));
                                    } else {
                                        Log.d("Update profile", "failed");
                                        new LovelyInfoDialog(mContext)
                                                .setTopColorRes(R.color.primary)
                                                .setTitle("False")
                                                .setMessage("False to update profile")
                                                .show();
                                    }

                                }
                            });
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

            }
        } else if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                Toast.makeText(mContext, "No Image available, please try again", Toast.LENGTH_LONG).show();
                return;
            }
            try {
                InputStream inputStream = mContext.getContentResolver().openInputStream(data.getData());

                Bitmap imgBitmap = BitmapFactory.decodeStream(inputStream);
                imgBitmap = ImageUtils.cropToSquare(imgBitmap);
                InputStream is = ImageUtils.convertBitmapToInputStream(imgBitmap);
                final Bitmap liteImage = ImageUtils.makeImageLite(is,
                        imgBitmap.getWidth(), imgBitmap.getHeight(),
                        ImageUtils.AVATAR_WIDTH, ImageUtils.AVATAR_HEIGHT);

                String imageBase64 = ImageUtils.encodeBase64(liteImage);
                myAccount.avata = imageBase64;

                waitingDialog.setCancelable(false)
                        .setTitle("Avatar updating....")
                        .setTopColorRes(R.color.colorPrimary)
                        .show();

                FirebaseDatabase.getInstance().getReference().child("user").child(StaticConfig.UID)
                        .child("avata").setValue(imageBase64)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {

                                    waitingDialog.dismiss();
                                    SharedPreferenceHelper preferenceHelper = SharedPreferenceHelper.getInstance(mContext);
                                    preferenceHelper.saveUserInfo(myAccount);
                                    profile_picture.setImageDrawable(ImageUtils.roundedImage(mContext, liteImage));

                                    new LovelyInfoDialog(mContext)
                                            .setTopColorRes(R.color.colorPrimary)
                                            .setTitle("Success")
                                            .setMessage("Update avatar successfully!")
                                            .show();
                                }
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                waitingDialog.dismiss();
                                Log.d("Update Avatar", "failed");
                                new LovelyInfoDialog(mContext)
                                        .setTopColorRes(R.color.colorAccent)
                                        .setTitle("False")
                                        .setMessage("False to update avatar")
                                        .show();
                            }
                        });
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }


    private void setImageAvatar(Context context, String imgBase64) {
        try {
            Resources res = getResources();
            Bitmap src;
            if (imgBase64.equals("default")) {
                src = BitmapFactory.decodeResource(res, R.drawable.default_avata);
            } else {
                byte[] decodedString = Base64.decode(imgBase64, Base64.DEFAULT);
                src = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            }

            profile_picture.setImageDrawable(ImageUtils.roundedImage(context, src));
            profile_picture.setDrawingCacheEnabled(true);
        } catch (Exception e) {
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent data = new Intent();
        data.putExtra("userName", myAccount.name);
        data.putExtra("avata", myAccount.avata);
        setResult(RESULT_OK, data);
        finish();
    }
}

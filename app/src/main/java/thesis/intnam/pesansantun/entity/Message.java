package thesis.intnam.pesansantun.entity;


import android.graphics.drawable.Drawable;

public class Message {
    public String id;
    public String idSender;
    public String idReceiver;
    public String text;
    public long timestamp;
    public Drawable avata;
    public Boolean isDisplayedTime;
    public String status;
    public String roomId;
}
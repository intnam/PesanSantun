package thesis.intnam.pesansantun;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import thesis.intnam.pesansantun.Utils.ImageUtils;
import thesis.intnam.pesansantun.data.SharedPreferenceHelper;
import thesis.intnam.pesansantun.data.StaticConfig;
import thesis.intnam.pesansantun.entity.User;

public class FriendsProfileActivity extends AppCompatActivity {

///
private static final String TAG = "Profile-Settings";
    private static final int REQUEST_UPLOAD_PHOTO = 1;
    private static final int PICK_IMAGE = 21;
    @BindView(R.id.profile_picture)
    protected CircleImageView profile_picture;
    @BindView(R.id.displayName)
    protected TextView displayName;


    private Context mContext;
    private User myAccount;
    private LovelyProgressDialog waitingDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.friends_profile);
        ButterKnife.bind(this);
        mContext = this;
        waitingDialog = new LovelyProgressDialog(mContext);
        //getSupportActionBar().setTitle("Settings");
        SharedPreferenceHelper prefHelper = SharedPreferenceHelper.getInstance(mContext);
        myAccount = prefHelper.getUserInfo();

        if (myAccount != null) {
            displayName.setText(myAccount.name);
            setImageAvatar(mContext, myAccount.avata);
        }

        retrieveDataFromFirebaseDB();


    }

    private void retrieveDataFromFirebaseDB() {
        FirebaseDatabase.getInstance().getReference().child("user").child(StaticConfig.UID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                myAccount = dataSnapshot.getValue(User.class);
                if(myAccount != null){
                    displayName.setText(myAccount.name);
                    setImageAvatar(mContext, myAccount.avata);
                    SharedPreferenceHelper preferenceHelper = SharedPreferenceHelper.getInstance(mContext);
                    preferenceHelper.saveUserInfo(myAccount);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setImageAvatar(Context context, String imgBase64) {
        try {
            Resources res = getResources();
            Bitmap src;
            if (imgBase64.equals("default")) {
                src = BitmapFactory.decodeResource(res, R.drawable.default_avata);
            } else {
                byte[] decodedString = Base64.decode(imgBase64, Base64.DEFAULT);
                src = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            }

            profile_picture.setImageDrawable(ImageUtils.roundedImage(context, src));
            profile_picture.setDrawingCacheEnabled(true);
        } catch (Exception e) {
        }
    }


}

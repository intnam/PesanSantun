package thesis.intnam.pesansantun.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Profile implements Parcelable {

    protected String key;
    protected String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Profile(String key, String value) {
        this.key = key;
        this.value = value;

    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.key);
        dest.writeString(this.value);

    }

    public Profile() {
    }

    protected Profile(Parcel in) {
        this.key = in.readString();
        this.value = in.readString();

    }

    public static final Creator<Profile> CREATOR = new Creator<Profile>() {
        @Override
        public Profile createFromParcel(Parcel source) {
            return new Profile(source);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }

    };
}

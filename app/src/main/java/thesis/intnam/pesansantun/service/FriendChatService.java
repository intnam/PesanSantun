package thesis.intnam.pesansantun.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Binder;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Base64;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import thesis.intnam.pesansantun.CommentActivity;
import thesis.intnam.pesansantun.MainActivity;
import thesis.intnam.pesansantun.R;
import thesis.intnam.pesansantun.data.PesanSantunDB;
import thesis.intnam.pesansantun.data.GroupDB;
import thesis.intnam.pesansantun.data.StaticConfig;
import thesis.intnam.pesansantun.entity.Comment;
import thesis.intnam.pesansantun.entity.Friend;
import thesis.intnam.pesansantun.entity.Group;
import thesis.intnam.pesansantun.entity.ListFriend;
import thesis.intnam.pesansantun.entity.Timeline;


public class FriendChatService extends Service {
    private static String TAG = "FriendChatService";
    // Binder given to clients
    public final IBinder mBinder = new LocalBinder();
    public Map<String, Boolean> mapMark;
    public Map<String, Query> mapQuery;
    public Map<String, ChildEventListener> mapChildEventListenerMap;
    public Map<String, Bitmap> mapBitmap;
    public ArrayList<String> listKey;
    public ListFriend listFriend;
    public ArrayList<Group> listGroup;
    public CountDownTimer updateOnline;
    public ChildEventListener eventListener;

    public FriendChatService() {
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mapMark = new HashMap<>();
        mapQuery = new HashMap<>();
        mapChildEventListenerMap = new HashMap<>();
        listFriend = PesanSantunDB.getInstance(this).getListFriendAll();
//        listGroup = GroupDB.getInstance(this).getListGroups();
        listKey = new ArrayList<>();
        mapBitmap = new HashMap<>();

        updateOnline = new CountDownTimer(System.currentTimeMillis(), StaticConfig.TIME_TO_REFRESH) {
            @Override
            public void onTick(long l) {
                ServiceUtils.updateBadWordLibrary(getApplicationContext());
                listFriend = PesanSantunDB.getInstance(getApplicationContext()).getListFriendAll();
            }

            @Override
            public void onFinish() {

            }
        };
        updateOnline.start();
        checkNotification(listFriend);

    }

    private void checkNotification(ListFriend listFriend) {
        for (final Friend friend : listFriend.getListFriend()) {


            eventListener = FirebaseDatabase.getInstance().getReference().child("message/" + friend.idRoom).limitToLast(1)
                    .addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            Map msgMap = (HashMap) dataSnapshot.getValue();
                            String id = dataSnapshot.getKey();
                            String idSender = (String) msgMap.get("idSender");
                            String status = (String) msgMap.get("status");

                            if (!StaticConfig.UID.equals(idSender) && status.equals("sent")) {
                                Boolean isAlreadyOnDevice = PesanSantunDB.getInstance(getApplicationContext())
                                        .checkMessageById(id);

                                if (!isAlreadyOnDevice) {
                                    String newMessage = (String) ((HashMap) dataSnapshot.getValue()).get("text");
                                    String avataString = friend.avata;
                                    Bitmap avata;
                                    if (!(StaticConfig.STR_DEFAULT_BASE64).equals(avataString)) {
                                        byte[] decodedString = Base64.decode(avataString, Base64.DEFAULT);
                                        avata = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                    } else {
                                        avata = BitmapFactory.decodeResource(getResources(), R.drawable.default_avata);
                                    }
                                    createNotify(friend.name, newMessage, friend.idRoom.hashCode(), avata, false);

                                }
                            }
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
            mapChildEventListenerMap.put(friend.id, eventListener);
        }
    }

    public void stopNotify(String id) {
        mapMark.put(id, false);
    }

    public void createNotify(String name, String content, int id, Bitmap icon, boolean isGroup) {
        Intent activityIntent = new Intent(this, MainActivity.class);
        activityIntent.putExtra("notification", "1");
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, activityIntent, PendingIntent.FLAG_ONE_SHOT);

        Uri sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.dropwater);

        NotificationCompat.Builder notificationBuilder = new
                NotificationCompat.Builder(this)
                .setLargeIcon(icon)
                .setContentTitle(name)
                .setContentText(content)
                .setContentIntent(pendingIntent)
                .setVibrate(new long[]{1000, 1000})
                .setSound(sound)
                .setAutoCancel(true);

        notificationBuilder.setSmallIcon(R.drawable.icon);

        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(
                        Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(id);
        notificationManager.notify(id,
                notificationBuilder.build());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "OnStartService");
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "OnBindService");
        return mBinder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for (final Friend friend : listFriend.getListFriend()) {

            if (mapChildEventListenerMap.get(friend.id) != null) {
                FirebaseDatabase.getInstance().getReference().child("message/" + friend.idRoom)
                        .removeEventListener(mapChildEventListenerMap.get(friend.id));
            }
        }
        updateOnline.cancel();
        Log.d(TAG, "OnDestroyService");
    }

    public class LocalBinder extends Binder {
        public FriendChatService getService() {
            // Return this instance of LocalService so clients can call public methods
            return FriendChatService.this;
        }
    }
}

package thesis.intnam.pesansantun.fragment;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.itemanimators.SlideDownAlphaAnimator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import thesis.intnam.pesansantun.CommentActivity;
import thesis.intnam.pesansantun.R;
import thesis.intnam.pesansantun.ToPostActivity;
import thesis.intnam.pesansantun.Utils.ImageUtils;
import thesis.intnam.pesansantun.adapter.CommentItem;
import thesis.intnam.pesansantun.adapter.TimelineItem;
import thesis.intnam.pesansantun.data.PesanSantunDB;
import thesis.intnam.pesansantun.data.SharedPreferenceHelper;
import thesis.intnam.pesansantun.data.StaticConfig;
import thesis.intnam.pesansantun.entity.Comment;
import thesis.intnam.pesansantun.entity.Friend;
import thesis.intnam.pesansantun.entity.ListFriend;
import thesis.intnam.pesansantun.entity.Timeline;
import thesis.intnam.pesansantun.entity.User;
import thesis.intnam.pesansantun.service.FriendChatService;
import thesis.intnam.pesansantun.service.ServiceUtils;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimelineFragment extends Fragment {
    private static final int REQUEST_CREATE_NEW_POST = 2;
    public static Map<String, ChildEventListener> mapChildEventListenerMap;
    protected static FastItemAdapter<TimelineItem> itemAdapter;
    private static LinearLayoutManager linearLayoutManager;
    private static ChildEventListener commentListener;
    public static Context mContext;
    @BindView(R.id.timelineList)
    protected RecyclerView timelineList;
    @BindView(R.id.whatsOnYourMind)
    protected LinearLayout whatsOnYourMind;
    @BindView(R.id.profile_picture)
    protected CircleImageView profile_picture;
    protected ListFriend listFriend;
    protected ArrayList<Timeline> timelineArrayList;
    private User myAccount;

    public TimelineFragment() {
        // Required empty public constructor
    }

    public static void onrefresh() {
        linearLayoutManager.scrollToPosition(0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.timeline_body, container, false);
        ButterKnife.bind(this, view);
        mContext = getActivity();
        findView();

        return view;
    }

    private void findView() {
        SharedPreferenceHelper prefHelper = SharedPreferenceHelper.getInstance(getActivity());
        myAccount = prefHelper.getUserInfo();
        setImageAvatar(getActivity(), myAccount.avata);
        itemAdapter = new FastItemAdapter<>();

        listFriend = PesanSantunDB.getInstance(getActivity()).getListFriendAll();
        Friend myUserId = new Friend();
        myUserId.id = StaticConfig.UID;
        listFriend.getListFriend().add(myUserId);

        for (Friend friend : listFriend.getListFriend()) {

            checkFriendTimeline(friend);
        }

        displayTimelineHistory();
        mapChildEventListenerMap = new HashMap<>();
        if (timelineArrayList != null) {
            for (Timeline timeline : timelineArrayList) {
                if (StaticConfig.UID.equals(timeline.getUserId()))
                    setCommentListener(timeline.getId());
            }
        }


    }

    private void setCommentListener(final String timelineId) {


        commentListener = FirebaseDatabase.getInstance().getReference().child("comment/" + timelineId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                HashMap mapComment = (HashMap) dataSnapshot.getValue();
                Comment comment = new Comment();

                comment.setUserId((String) mapComment.get("userId"));
                comment.setCommentText((String) mapComment.get("commentText"));
                if (mapComment.get("timestamp") != null)
                    comment.setTimestamp((Long) mapComment.get("timestamp"));

                notifyComment(comment, timelineId);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        mapChildEventListenerMap.put(timelineId, commentListener);

    }

    public void notifyComment(Comment comment, String timelineId) {
        Intent activityIntent = new Intent(mContext, CommentActivity.class);
        activityIntent.putExtra("timelineId", timelineId);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, activityIntent, PendingIntent.FLAG_ONE_SHOT);
        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.default_avata);
        Uri sound = Uri.parse("android.resource://" + mContext.getPackageName() + "/" + R.raw.dropwater);

        NotificationCompat.Builder notificationBuilder = new
                NotificationCompat.Builder(mContext)
                .setLargeIcon(icon)
                .setContentTitle("new comment")
                .setContentText(comment.getCommentText())
                .setContentIntent(pendingIntent)
                .setVibrate(new long[]{1000, 1000})
                .setSound(sound)
                .setAutoCancel(true);

        notificationBuilder.setSmallIcon(R.drawable.icon);

        NotificationManager notificationManager =
                (NotificationManager) mContext.getSystemService(
                        Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(0);
        notificationManager.notify(0,
                notificationBuilder.build());
    }

    @OnClick(R.id.whatsOnYourMind)
    protected void postStatus() {
        Intent intent = new Intent(mContext, ToPostActivity.class);

        startActivityForResult(intent, REQUEST_CREATE_NEW_POST);
    }

    private void displayTimelineHistory() {
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        timelineArrayList = PesanSantunDB.getInstance(mContext).getTimelines();

        for (Timeline timeline : timelineArrayList) {
            itemAdapter.add(new TimelineItem(timeline));
        }

        timelineList.setLayoutManager(linearLayoutManager);
        timelineList.setAdapter(itemAdapter);
        timelineList.setNestedScrollingEnabled(true);


    }

    public static void checkFriendTimeline(Friend friend) {
        FirebaseDatabase.getInstance().getReference().child("timeline/" + friend.id).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String id = dataSnapshot.getKey();
                Boolean isAlreadyOnDevice = PesanSantunDB.getInstance(mContext).checkTimelineById(id);

                if (!isAlreadyOnDevice) {

                    HashMap mapTimeline = (HashMap) dataSnapshot.getValue();
                    Timeline newTimeline = new Timeline();
                    newTimeline.setId(id);
                    newTimeline.setUserId((String) mapTimeline.get("userId"));
                    newTimeline.setTextStatus((String) mapTimeline.get("textStatus"));
                    if (mapTimeline.get("timestamp") != null)
                        newTimeline.setTimestamp((Long) mapTimeline.get("timestamp"));


                    itemAdapter.add(0, new TimelineItem(newTimeline));
                    itemAdapter.notifyDataSetChanged();
                    PesanSantunDB.getInstance(mContext).addTimeline(newTimeline);
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setImageAvatar(Context context, String imgBase64) {
        try {
            Resources res = getResources();
            Bitmap src;
            if (imgBase64.equals("default")) {
                src = BitmapFactory.decodeResource(res, R.drawable.default_avata);
            } else {
                byte[] decodedString = Base64.decode(imgBase64, Base64.DEFAULT);
                src = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            }

            profile_picture.setImageDrawable(ImageUtils.roundedImage(context, src));
        } catch (Exception e) {
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CREATE_NEW_POST && resultCode == RESULT_OK) {

        }
    }
}

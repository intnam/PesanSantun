package thesis.intnam.pesansantun;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import thesis.intnam.pesansantun.data.SharedPreferenceHelper;
import thesis.intnam.pesansantun.data.StaticConfig;
import thesis.intnam.pesansantun.entity.Timeline;
import thesis.intnam.pesansantun.entity.User;
import thesis.intnam.pesansantun.service.ServiceUtils;

public class ToPostActivity extends AppCompatActivity {

    @BindView(R.id.status)
    protected EditText status;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.to_posting_page);
        ButterKnife.bind(this);
        mContext = this;
        getSupportActionBar().hide();

    }

    @OnClick(R.id.postButton)
    protected void postStatus() {

        String post = status.getText().toString();

        if (post.isEmpty()) {
            Toast.makeText(mContext, getString(R.string.post_cant_be_empty), Toast.LENGTH_LONG).show();

        } else {
            String resultText = ServiceUtils.checkBadWords(post, mContext);

            if (resultText.contains("***")) {
                status.setText(resultText);
                Toast.makeText(mContext, getString(R.string.post_cant_be_contained_badword), Toast.LENGTH_LONG).show();
            } else {
                Timeline newTimeline = new Timeline();

                newTimeline.setUserId(StaticConfig.UID);
                newTimeline.setTextStatus(status.getText().toString());
                newTimeline.setTimestamp(System.currentTimeMillis());

                FirebaseDatabase.getInstance().getReference().child("timeline/" + StaticConfig.UID).push().setValue(newTimeline);

                Intent result = new Intent();
                setResult(RESULT_OK, result);
                this.finish();
            }
        }
    }


}

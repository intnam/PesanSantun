package thesis.intnam.pesansantun.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;
import com.yarolegovich.lovelydialog.LovelyTextInputDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import thesis.intnam.pesansantun.ChatActivity;
import thesis.intnam.pesansantun.MainActivity;
import thesis.intnam.pesansantun.R;
import thesis.intnam.pesansantun.adapter.ChatListItem;
import thesis.intnam.pesansantun.adapter.ConversationItem;
import thesis.intnam.pesansantun.adapter.FriendListItem;
import thesis.intnam.pesansantun.data.PesanSantunDB;
import thesis.intnam.pesansantun.data.StaticConfig;
import thesis.intnam.pesansantun.entity.BadWord;
import thesis.intnam.pesansantun.entity.ChatList;
import thesis.intnam.pesansantun.entity.Friend;
import thesis.intnam.pesansantun.entity.ListFriend;
import thesis.intnam.pesansantun.entity.Message;
import thesis.intnam.pesansantun.service.ServiceUtils;


public class ChatFragment extends Fragment implements MainActivity.OnBackPressedListener {

    public static int ACTION_START_CHAT = 1;
    public static Map<String, ChildEventListener> mapChildEventListenerMap;
    public static Context context;
    static ArrayList<Friend> friendList;
    private static FastItemAdapter<ChatListItem> fastItemAdapter;
    private static DatabaseReference mDatabaseReference;
    private static ChildEventListener lastMessageListener;
    private static Calendar lastMsgTime;
    private static String idRoomOnDetail;
    public FragChatFloatClickButton onClickFloatButton;
    private RecyclerView recyclerChatList;
    private ListFriend dataListFriendByMessage = null;
    private LinearLayoutManager linearLayoutManager;
    private List<ChatListItem> chatListItems;
    private Boolean islistOnLongPressed;
    private int selectedItemPosition;

    public ChatFragment() {

        onClickFloatButton = new FragChatFloatClickButton();
    }

    private static void checkLastMessage(final Friend friend) {

        lastMessageListener = FirebaseDatabase.getInstance().getReference().child("message/" + friend.idRoom)
                .addChildEventListener(new ChildEventListener() {

                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        String id = dataSnapshot.getKey();
                        HashMap mapMessage = (HashMap) dataSnapshot.getValue();

                        final Message newMessage = new Message();
                        newMessage.id = dataSnapshot.getKey();
                        newMessage.idSender = (String) mapMessage.get("idSender");
                        newMessage.idReceiver = (String) mapMessage.get("idReceiver");
                        newMessage.text = (String) mapMessage.get("text");
                        newMessage.timestamp = (long) mapMessage.get("timestamp");
                        newMessage.roomId = friend.idRoom;
                        newMessage.status = (String) mapMessage.get("status");

                        String resultText = ServiceUtils.checkBadWords(newMessage.text, context);

                        if (resultText.contains("***")) {
                            newMessage.status = "blocked";
                            newMessage.text = resultText;
                            setMessageStatusIntoFirebase(newMessage);
                        } else {
                            Boolean isAlreadyOnDevice = PesanSantunDB.getInstance(context)
                                    .checkMessageById(dataSnapshot.getKey());

                            if (!isAlreadyOnDevice && newMessage.text != null) {

                                if (!isAlreadyOnDevice) {
                                    saveMessageIntoDevice(newMessage);
                                }

                                if (idRoomOnDetail != null && idRoomOnDetail.equals(newMessage.roomId)) {

                                    if (!isAlreadyOnDevice) {

                                        if (newMessage.idSender.equals(StaticConfig.UID)) {
                                            newMessage.status = "sent";
                                        } else {
                                            newMessage.status = "read";
                                            setMessageStatusIntoFirebase(newMessage);
                                        }
                                        ChatActivity.addDetailMessage(newMessage);

                                    }
                                } else {
                                    ChatList chatList = new ChatList();
                                    chatList.setId(friend.id);
                                    chatList.setName(friend.name);
                                    chatList.setIdRoom(friend.idRoom);
                                    chatList.setSenderId((String) mapMessage.get("idSender"));
                                    chatList.setAvata(friend.avata);
                                    chatList.setChatText((String) mapMessage.get("text"));
                                    chatList.setMessageStatus((String) mapMessage.get("status"));
                                    chatList.setTimestamp((long) mapMessage.get("timestamp"));

                                    String time = new SimpleDateFormat("EEE, d MMM yyyy").format(new Date((long) mapMessage.get("timestamp")));
                                    String today = new SimpleDateFormat("EEE, d MMM yyyy").format(new Date(System.currentTimeMillis()));
                                    if (today.equals(time)) {
                                        chatList.setTxtTime(new SimpleDateFormat("HH:mm").format(new Date((long) mapMessage.get("timestamp"))));
                                    } else {
                                        chatList.setTxtTime(new SimpleDateFormat("d MMM").format(new Date((long) mapMessage.get("timestamp"))));
                                    }

                                    includeLastChatIntoChatListItem(chatList);
                                    if (!isAlreadyOnDevice) {

                                        if (newMessage.status.equals("sent")) {
                                            ServiceUtils.playRingtone(context);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        HashMap mapMessage = (HashMap) dataSnapshot.getValue();
                        Message message = new Message();
                        message.id = dataSnapshot.getKey();
                        message.idSender = (String) mapMessage.get("idSender");
                        message.status = (String) mapMessage.get("status");
                        message.text = (String) mapMessage.get("text");

                        ChatActivity.updateDetailMessage(message);
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        mapChildEventListenerMap.put(friend.id, lastMessageListener);
    }

    private static void includeLastChatIntoChatListItem(ChatList chatList) {
        Boolean isOnTheList = false;
        int fromPost = 0;
        int toPost = 0;

        ChatListItem chatListItem = new ChatListItem(chatList);
        Calendar newMsgTime = Calendar.getInstance();
        newMsgTime.setTimeInMillis(chatList.getTimestamp());

        if (fastItemAdapter.getAdapterItems().isEmpty()) {
            fastItemAdapter.add(chatListItem);
        } else {

            for (int i = 0; i < fastItemAdapter.getAdapterItems().size(); i++) {
                ChatListItem item = fastItemAdapter.getAdapterItem(i);
                Calendar existingTime = Calendar.getInstance();
                existingTime.setTimeInMillis(item.chatList.getTimestamp());

                if (chatList.getId().equals(item.chatList.getId())) {
                    isOnTheList = true;
                    fromPost = i;

                }
                if (newMsgTime.before(existingTime)) {
                    toPost = toPost == 0 ? toPost : toPost++;
                }
            }

            if (isOnTheList) {
                ChatList selectedChatList = fastItemAdapter.getAdapterItem(fromPost).chatList;
                selectedChatList.setId(chatList.getId());
                selectedChatList.setName(chatList.getName());
                selectedChatList.setIdRoom(chatList.getIdRoom());
                selectedChatList.setChatText(chatList.getChatText());
                selectedChatList.setTimestamp(chatList.getTimestamp());
                selectedChatList.setTxtTime(chatList.getTxtTime());
                selectedChatList.setAvata(chatList.getAvata());
                selectedChatList.setMessageStatus(chatList.getMessageStatus());
                selectedChatList.setSenderId(chatList.getSenderId());
                fastItemAdapter.notifyDataSetChanged();
                fastItemAdapter.move(fromPost, toPost);
            } else {
                fastItemAdapter.add(toPost, chatListItem);
                fastItemAdapter.notifyDataSetChanged();
            }

        }
    }

    private static void setMessageStatusIntoFirebase(Message message) {

        FirebaseDatabase.getInstance().getReference().child("message/" + message.roomId + "/" + message.id + "/text").setValue(message.text);
        FirebaseDatabase.getInstance().getReference().child("message/" + message.roomId + "/" + message.id + "/status").setValue(message.status);
    }

    private static void saveMessageIntoDevice(Message newMessage) {
        PesanSantunDB.getInstance(context).addMessage(newMessage);
    }

    public static void setIsOnDetailPage(String roomId) {
        idRoomOnDetail = roomId;
    }

    public static void enableListenerByIdFriend(String idFriend) {

        for (Friend friend : friendList) {
            if (friend.id.equals(idFriend)) {
                checkLastMessage(friend);
                break;
            }
        }

    }

    public static void updateChatList(Friend friend) {

        for (ChatListItem item : fastItemAdapter.getAdapterItems()) {

            ChatList chatlist = item.chatList;

            if (chatlist.getId().equals(friend.id)) {
                chatlist.setName(friend.name);
                chatlist.setAvata(friend.avata);
                fastItemAdapter.notifyDataSetChanged();
                break;
            }
        }
        if (mapChildEventListenerMap.get(friend.id) == null) {
            checkLastMessage(friend);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_chat, container, false);
        recyclerChatList = (RecyclerView) layout.findViewById(R.id.recycleChatList);
        fastItemAdapter = new FastItemAdapter<>();
        chatListItems = new ArrayList<>();
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerChatList.setLayoutManager(linearLayoutManager);
        islistOnLongPressed = false;
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mapChildEventListenerMap = new HashMap<>();
        context = layout.getContext();
        checkNewMessageFromFriend();
        return layout;
    }

    private void checkNewMessageFromFriend() {
        friendList = PesanSantunDB.getInstance(getContext()).getAllFriendList();
        for (Friend friend : friendList) {
            checkLastMessage(friend);
        }
    }

    private void setDataChatList() {

        ArrayList<Message> lattestMessageList = PesanSantunDB.getInstance(getContext()).getLatestMessageFromAllFriends();
        for (Message message : lattestMessageList) {
            if (message.roomId != null) {
                Friend friend = PesanSantunDB.getInstance(getContext()).getFriendByIdRoom(message.roomId);
                if (friend != null) {
                    ChatList chatList = new ChatList();
                    chatList.setId(friend.id);
                    chatList.setName(friend.name);
                    chatList.setIdRoom(friend.idRoom);
                    chatList.setAvata(friend.avata);

                    chatList.setChatText(message.text);
                    chatList.setMessageStatus(message.status);
                    chatList.setSenderId(message.idSender);
                    chatList.setTimestamp(message.timestamp);

                    String time = new SimpleDateFormat("EEE, d MMM yyyy").format(new Date(message.timestamp));
                    String today = new SimpleDateFormat("EEE, d MMM yyyy").format(new Date(System.currentTimeMillis()));
                    if (today.equals(time)) {
                        chatList.setTxtTime(new SimpleDateFormat("HH:mm").format(new Date(message.timestamp)));
                    } else {
                        chatList.setTxtTime(new SimpleDateFormat("d MMM").format(new Date(message.timestamp)));
                    }
                    includeLastChatIntoChatListItem(chatList);
                }

            }

        }

        fastItemAdapter.add(chatListItems);
        linearLayoutManager.scrollToPosition(fastItemAdapter.getAdapterItemCount() - 1);
        fastItemAdapter.notifyDataSetChanged();
        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<ChatListItem>() {
            @Override
            public boolean onClick(View view, IAdapter<ChatListItem> adapter, ChatListItem item, int position) {


                TextView txtMessage = (TextView) view.findViewById(R.id.txtMessage);
                txtMessage.setTypeface(Typeface.DEFAULT);
                ChatList chatList = item.chatList;

                Intent intent = new Intent(getContext(), ChatActivity.class);
                intent.putExtra(StaticConfig.INTENT_KEY_CHAT_FRIEND, chatList.getName());
                ArrayList<CharSequence> idFriend = new ArrayList<CharSequence>();
                idFriend.add(chatList.getId());
                intent.putCharSequenceArrayListExtra(StaticConfig.INTENT_KEY_CHAT_ID, idFriend);
                intent.putExtra(StaticConfig.INTENT_KEY_CHAT_ROOM_ID, chatList.getIdRoom());
                startActivityForResult(intent, ChatFragment.ACTION_START_CHAT);
                return true;
            }
        });

        fastItemAdapter.withOnLongClickListener(new FastAdapter.OnLongClickListener<ChatListItem>() {
            @Override
            public boolean onLongClick(View view, IAdapter<ChatListItem> adapter, ChatListItem item, final int position) {

                islistOnLongPressed = true;
                selectedItemPosition = position;

                final LinearLayout chatItem_layout = (LinearLayout) view.findViewById(R.id.chatItem_layout);
                final LinearLayout deleteChat_layout = (LinearLayout) view.findViewById(R.id.deleteChat_layout);
                final ChatList chatList = item.chatList;
                deleteChat_layout.setVisibility(View.VISIBLE);
                chatItem_layout.setBackgroundColor(getResources().getColor(R.color.grey_text));


                deleteChat_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new AlertDialog.Builder(getContext())
                                .setTitle("Delete Chat")
                                .setMessage("Are you sure want to delete this chat ?")
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        chatItem_layout.setBackgroundColor(getResources().getColor(R.color.white));
                                        deleteChat_layout.setVisibility(View.GONE);

                                        PesanSantunDB.getInstance(getContext()).deleteChatFromDevicesByRoomId(chatList.getIdRoom());
                                        deleteReadMessagefromServer(chatList.getIdRoom());
                                        fastItemAdapter.remove(position);
                                        islistOnLongPressed = false;
                                    }

                                })
                                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        chatItem_layout.setBackgroundColor(getResources().getColor(R.color.white));
                                        deleteChat_layout.setVisibility(View.GONE);
                                    }
                                }).show();

                    }
                });

                return true;
            }

        });

        recyclerChatList.setAdapter(fastItemAdapter);
    }

    private void deleteReadMessagefromServer(String roomId) {
        FirebaseDatabase.getInstance().getReference().child("message")
                .child(roomId).removeValue();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((MainActivity) getActivity()).setOnBackPressedListener(this);
    }

    @Override
    public void doBack() {
        if (islistOnLongPressed) {
            ChatListItem selectedItem = fastItemAdapter.getAdapterItem(selectedItemPosition);
            fastItemAdapter.set(selectedItemPosition, selectedItem);
            fastItemAdapter.notifyDataSetChanged();
            islistOnLongPressed = false;
        } else {
            ((MainActivity) getActivity()).setOnBackPressedListener(null);

            for (Friend friend : friendList) {
                FirebaseDatabase.getInstance().getReference().child("message/" + friend.idRoom).limitToLast(1).removeEventListener(lastMessageListener);
            }
            super.getActivity().onBackPressed();
        }
    }

    @Override
    public void onDestroy() {

        ((MainActivity) getActivity()).setOnBackPressedListener(null);

        for (Friend friend : friendList) {
            mDatabaseReference.child("message/" + friend.idRoom)
                    .limitToLast(1).removeEventListener(mapChildEventListenerMap.get(friend.id));
        }
        lastMessageListener = null;
        super.onDestroy();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        setDataChatList();
        super.onResume();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (ACTION_START_CHAT == requestCode && data != null) {

            String idFriend = data.getStringExtra("idFriend");

            for (Friend friend : friendList) {
                if (friend.id.equals(idFriend)) {
                    checkLastMessage(friend);
                    break;
                }
            }

            idRoomOnDetail = null;
        }
    }

    public class FragChatFloatClickButton implements View.OnClickListener {
        Context context;
        LovelyProgressDialog dialogWait;

        public FragChatFloatClickButton() {
        }

        public ChatFragment.FragChatFloatClickButton getInstance(Context context) {
            this.context = context;
            dialogWait = new LovelyProgressDialog(context);
            return this;
        }

        @Override
        public void onClick(final View view) {

            final AlertDialog alertDialog;

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setView(getLayoutInflater().inflate(R.layout.dialog_add_badword, null));
            alertDialog = builder.create();
            alertDialog.setCancelable(false);
            alertDialog.show();


            Button cancelButton = (Button) alertDialog.findViewById(R.id.cancel);
            Button okButton = (Button) alertDialog.findViewById(R.id.ok);

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AutoCompleteTextView newBadWordText = (AutoCompleteTextView) alertDialog.findViewById(R.id.newBadWord);
                    String aNewBadWord = newBadWordText.getText().toString();

                    if (aNewBadWord.isEmpty()) {
                        newBadWordText.setError(getResources().getString(R.string.please_input_badword));
                    } else {
                        Boolean isExist = PesanSantunDB.getInstance(context).checkBadWords(aNewBadWord.toLowerCase());

                        if (!isExist) {
                            BadWord badWord = new BadWord();
                            badWord.setValue(aNewBadWord.toLowerCase());
                            badWord.setCreatedTimestamp(System.currentTimeMillis());
                            PesanSantunDB.getInstance(context).addBadWords(badWord);
                            Toast.makeText(context, getResources().getString(R.string.badword_has_successfully_added), Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                        } else {
                            newBadWordText.setError(getResources().getString(R.string.badword_already_exist));
                        }
                    }

                }
            });
        }

    }
}



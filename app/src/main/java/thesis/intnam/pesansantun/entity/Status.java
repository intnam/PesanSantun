package thesis.intnam.pesansantun.entity;


public class Status {
    public boolean isOnline;
    public long timestamp;

    public Status() {
        isOnline = false;
        timestamp = 0;
    }
}

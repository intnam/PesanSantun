package thesis.intnam.pesansantun;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import thesis.intnam.pesansantun.data.PesanSantunDB;
import thesis.intnam.pesansantun.data.StaticConfig;
import thesis.intnam.pesansantun.entity.Friend;
import thesis.intnam.pesansantun.fragment.ChatFragment;
import thesis.intnam.pesansantun.fragment.FriendsFragment;
import thesis.intnam.pesansantun.fragment.ProfileFragment;
import thesis.intnam.pesansantun.fragment.TimelineFragment;
import thesis.intnam.pesansantun.service.ServiceUtils;

public class MainActivity extends AppCompatActivity {
    public static String STR_CHAT_FRAGMENT = "Chat";
    public static String STR_CONTACT_FRAGMENT = "Contact";
    public static String STR_TIMELINE_FRAGMENT = "Timeline";
    public static String STR_PROFILE_FRAGMENT = "Profile";
    private static String TAG = "MainActivity";
    protected OnBackPressedListener onBackPressedListener;
    private ViewPager viewPager;
    private TabLayout tabLayout = null;
    private FloatingActionButton floatButton;
    private ViewPagerAdapter adapter;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser user;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        Intent intentData = getIntent();
        String selectedPosition = intentData.getStringExtra("notification");

        if (selectedPosition != null)
            viewPager.setCurrentItem(Integer.parseInt(selectedPosition));

        floatButton = (FloatingActionButton) findViewById(R.id.fab);
        initTab();
        checkUserAuthentication();
        ServiceUtils.updateBadWordLibrary(getApplicationContext());
        checkNewFriend(StaticConfig.UID);
    }

    private void checkUserAuthentication() {

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    StaticConfig.UID = user.getUid();

                } else {
                    MainActivity.this.finish();
                    // User is signed in
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };
    }

    private void checkNewFriend(String UID) {
        FirebaseDatabase.getInstance().getReference().child("friend/" + UID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<String> listFriendId = new ArrayList();
                if (dataSnapshot.getValue() != null) {
                    HashMap mapRecord = (HashMap) dataSnapshot.getValue();
                    Iterator listKey = mapRecord.keySet().iterator();
                    while (listKey.hasNext()) {
                        String key = listKey.next().toString();
                        String friendId = mapRecord.get(key).toString();

                        listFriendId.add(friendId);
                    }
                    getFriendInfo(listFriendId);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void getFriendInfo(final ArrayList<String> listFriendID) {

        for (final String id : listFriendID) {

            FirebaseDatabase.getInstance().getReference().child("user/" + id).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() != null) {
                        Friend friend = new Friend();
                        HashMap mapUserInfo = (HashMap) dataSnapshot.getValue();
                        friend.name = (String) mapUserInfo.get("name");
                        friend.email = (String) mapUserInfo.get("email");
                        friend.avata = (String) mapUserInfo.get("avata");
                        friend.id = id;
                        friend.idRoom = id.compareTo(StaticConfig.UID) > 0 ? (StaticConfig.UID + id).hashCode() + "" : "" + (id + StaticConfig.UID).hashCode();

                        PesanSantunDB.getInstance(mContext).saveFriendInfo(friend);

                        ChatFragment.updateChatList(friend);
                        FriendsFragment.setFriendList(friend);
                        TimelineFragment.checkFriendTimeline(friend);

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }


    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
        ServiceUtils.stopServiceFriendChat(getApplicationContext(), false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private void initTab() {
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.white));
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
        floatButton.setOnClickListener(((ChatFragment) adapter.getItem(0)).onClickFloatButton.getInstance(MainActivity.this));
        floatButton.setImageResource(R.drawable.ic_library_books);
    }


    private void setupTabIcons() {
        int[] tabIcons = {
                R.drawable.ic_chat_white_18dp,
                R.drawable.ic_person_white_24dp,
                R.drawable.ic_public_white_18dp,
                R.drawable.ic_home_white_24dp
        };

        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new ChatFragment(), STR_CHAT_FRAGMENT);
        adapter.addFrag(new FriendsFragment(), STR_CONTACT_FRAGMENT);
        adapter.addFrag(new TimelineFragment(), STR_TIMELINE_FRAGMENT);
        adapter.addFrag(new ProfileFragment(), STR_PROFILE_FRAGMENT);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                setHeaderTitle(position);
            }

            @Override
            public void onPageSelected(int position) {
                setHeaderTitle(position);
                ServiceUtils.stopServiceFriendChat(MainActivity.this.getApplicationContext(), false);
                if (adapter.getItem(position) instanceof FriendsFragment) {
                    floatButton.setVisibility(View.VISIBLE);
                    floatButton.setOnClickListener(((FriendsFragment) adapter.getItem(position)).onClickFloatButton.getInstance(MainActivity.this));
                    floatButton.setImageResource(R.drawable.plus);
                } else if (adapter.getItem(position) instanceof ChatFragment) {
                    floatButton.setVisibility(View.VISIBLE);
                    floatButton.setOnClickListener(((ChatFragment) adapter.getItem(position)).onClickFloatButton.getInstance(MainActivity.this));
                    floatButton.setImageResource(R.drawable.ic_library_books);
                } else if (adapter.getItem(position) instanceof TimelineFragment) {
                    floatButton.setVisibility(View.GONE);
                    TimelineFragment.onrefresh();
                } else if (adapter.getItem(position) instanceof ProfileFragment) {
                    floatButton.setVisibility(View.GONE);
                    ProfileFragment.onrefresh();
                } else {
                    floatButton.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    private void setHeaderTitle(int position) {
        switch (position) {
            case 0:
                getSupportActionBar().setTitle(getString(R.string.tab_1));

                break;
            case 1:
                getSupportActionBar().setTitle(getString(R.string.tab_2));
                break;
            case 2:
                getSupportActionBar().setTitle(getString(R.string.tab_3));
                break;
            case 3:
                getSupportActionBar().setTitle(getString(R.string.tab_4));
                break;
            default:
                throw new IllegalStateException("Invalid position: " + position);
        }
    }

    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }

    @Override
    protected void onDestroy() {
        onBackPressedListener = null;
        super.onDestroy();
        ServiceUtils.startServiceFriendChat(getApplicationContext());
    }

    @Override
    public void onBackPressed() {
        if (onBackPressedListener != null) {
            onBackPressedListener.doBack();
        } else {
            super.onBackPressed();
        }
    }

    public interface OnBackPressedListener {
        void doBack();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return null;
        }

    }
}
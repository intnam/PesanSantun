package thesis.intnam.pesansantun;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import java.io.FileNotFoundException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import thesis.intnam.pesansantun.Utils.ImageUtils;
import thesis.intnam.pesansantun.data.SharedPreferenceHelper;
import thesis.intnam.pesansantun.data.StaticConfig;
import thesis.intnam.pesansantun.entity.User;

public class ProfileInfoActivity extends AppCompatActivity {

    private static final String TAG = "Profile-Info";
    private static final int REQUEST_UPLOAD_PHOTO = 1;
    @BindView(R.id.displayName)
    protected EditText displayName;
    @BindView(R.id.profile_picture)
    protected CircleImageView profile_picture;
    private Context mContext;
    private StorageReference mStorageRef;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private Uri photoUrl;
    private LovelyProgressDialog waitingDialog;
    private User myAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_info_page);
        ButterKnife.bind(this);
        mContext = this;
        getSupportActionBar().hide();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        SharedPreferenceHelper prefHelper = SharedPreferenceHelper.getInstance(mContext);
        myAccount = prefHelper.getUserInfo();

        if (myAccount == null) {
            myAccount = new User();
        }


    }


    @OnClick(R.id.profile_picture)
    public void uploadPhoto() {
        Matisse.from(this)
                .choose(MimeType.of(MimeType.JPEG, MimeType.PNG))
                .countable(false)
//                .showSingleMediaType(true)
                .maxSelectable(1)
                .theme(R.style.Matisse_Dracula)
//                .addFilter(new GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
//                .gridExpectedSize(getResources().getDimensionPixelSize(R.dimen.grid_expected_size))
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                .thumbnailScale(0.85f)
                .imageEngine(new GlideEngine())
                .forResult(REQUEST_UPLOAD_PHOTO);
    }


    @OnClick(R.id.id_buttonNext_profileInfo)
    public void addProfileInfo() {

        if (validation()) {

            final String newName = displayName.getText().toString();

            FirebaseDatabase.getInstance().getReference().child("user").child(StaticConfig.UID)
                    .child("name").setValue(newName).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        myAccount.name = newName;
                        displayName.setText(myAccount.name);
                        SharedPreferenceHelper prefHelper = SharedPreferenceHelper.getInstance(mContext);
                        prefHelper.saveUserInfo(myAccount);
                        Intent intent = new Intent(mContext, MainActivity.class);
                        startActivity(intent);
                        Log.d(TAG, "User profile updated.");
                    }
                }
            });

        }

    }

    private boolean validation() {

        if (displayName.getText().toString().isEmpty()) {
            displayName.setError(getString(R.string.empty_displayName_error));
            return false;
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_UPLOAD_PHOTO && resultCode == RESULT_OK) {
            Log.d("Matisse", "Uris: " + Matisse.obtainResult(data));
            CropImage.activity(Matisse.obtainResult(data).get(0))
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .setAspectRatio(1, 1)
                    .start(this);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            waitingDialog.setCancelable(false)
                    .setTitle("updating the profile....")
                    .setTopColorRes(R.color.primary)
                    .show();

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                Uri file = result.getUri();
                try {

                    InputStream inputStream = mContext.getContentResolver().openInputStream(file);

                    Bitmap imgBitmap = BitmapFactory.decodeStream(inputStream);
                    imgBitmap = ImageUtils.cropToSquare(imgBitmap);
                    InputStream is = ImageUtils.convertBitmapToInputStream(imgBitmap);
                    final Bitmap liteImage = ImageUtils.makeImageLite(is,
                            imgBitmap.getWidth(), imgBitmap.getHeight(),
                            ImageUtils.AVATAR_WIDTH, ImageUtils.AVATAR_HEIGHT);

                    final String imageBase64 = ImageUtils.encodeBase64(liteImage);

                    FirebaseDatabase.getInstance().getReference().child("user").child(StaticConfig.UID)
                            .child("avata").setValue(imageBase64)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    waitingDialog.dismiss();
                                    if (task.isSuccessful()) {
                                        SharedPreferenceHelper preferenceHelper = SharedPreferenceHelper.getInstance(mContext);
                                        myAccount.avata = imageBase64;
                                        preferenceHelper.saveUserInfo(myAccount);
                                        profile_picture.setImageDrawable(ImageUtils.roundedImage(mContext, liteImage));
                                    } else {
                                        Log.d("Update profile", "failed");
                                        new LovelyInfoDialog(mContext)
                                                .setTopColorRes(R.color.primary)
                                                .setTitle("False")
                                                .setMessage("False to update profile")
                                                .show();
                                    }

                                }
                            });
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

            }
        }
    }

}

package thesis.intnam.pesansantun.entity;

import java.util.ArrayList;


public class ListMessage {
    private ArrayList<Message> listMessage;

    public ListMessage() {
        listMessage = new ArrayList<>();
    }

    public ArrayList<Message> getListMessage() {
        return listMessage;
    }

    public void setListFriend(ArrayList<Message> listMessage) {
        this.listMessage = listMessage;
    }

}


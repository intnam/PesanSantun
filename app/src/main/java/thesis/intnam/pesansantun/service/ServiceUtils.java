package thesis.intnam.pesansantun.service;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.opencsv.CSVReader;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import thesis.intnam.pesansantun.R;
import thesis.intnam.pesansantun.data.PesanSantunDB;
import thesis.intnam.pesansantun.data.SharedPreferenceHelper;
import thesis.intnam.pesansantun.data.StaticConfig;
import thesis.intnam.pesansantun.entity.BadWord;
import thesis.intnam.pesansantun.entity.Friend;
import thesis.intnam.pesansantun.entity.ListFriend;


public class ServiceUtils {

    private static ServiceConnection connectionServiceFriendChatForStart = null;
    private static ServiceConnection connectionServiceFriendChatForDestroy = null;
    private HashMap specialMap;

    public static boolean isServiceFriendChatRunning(Context context) {
        Class<?> serviceClass = FriendChatService.class;
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void stopServiceFriendChat(Context context, final boolean kill) {
        if (isServiceFriendChatRunning(context)) {
            Intent intent = new Intent(context, FriendChatService.class);
            if (connectionServiceFriendChatForDestroy != null) {
                context.unbindService(connectionServiceFriendChatForDestroy);
            }
            connectionServiceFriendChatForDestroy = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName className,
                                               IBinder service) {
                    FriendChatService.LocalBinder binder = (FriendChatService.LocalBinder) service;
                    binder.getService().stopSelf();
                }

                @Override
                public void onServiceDisconnected(ComponentName arg0) {
                }
            };
            context.bindService(intent, connectionServiceFriendChatForDestroy, Context.BIND_NOT_FOREGROUND);
        }
    }

    public static void stopRoom(Context context, final String idRoom) {
        if (isServiceFriendChatRunning(context)) {
            Intent intent = new Intent(context, FriendChatService.class);
            if (connectionServiceFriendChatForDestroy != null) {
                context.unbindService(connectionServiceFriendChatForDestroy);
                connectionServiceFriendChatForDestroy = null;
            }
            connectionServiceFriendChatForDestroy = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName className,
                                               IBinder service) {
                    FriendChatService.LocalBinder binder = (FriendChatService.LocalBinder) service;
                    binder.getService().stopNotify(idRoom);
                }

                @Override
                public void onServiceDisconnected(ComponentName arg0) {
                }
            };
            context.bindService(intent, connectionServiceFriendChatForDestroy, Context.BIND_NOT_FOREGROUND);
        }
    }

    public static void startServiceFriendChat(Context context) {
        if (!isServiceFriendChatRunning(context)) {
            Intent myIntent = new Intent(context, FriendChatService.class);
            context.startService(myIntent);
        } else {
            if (connectionServiceFriendChatForStart != null) {
                context.unbindService(connectionServiceFriendChatForStart);
            }
            connectionServiceFriendChatForStart = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName className,
                                               IBinder service) {
                    FriendChatService.LocalBinder binder = (FriendChatService.LocalBinder) service;
                    for (Friend friend : binder.getService().listFriend.getListFriend()) {
                        binder.getService().mapMark.put(friend.idRoom, true);
                    }
                }

                @Override
                public void onServiceDisconnected(ComponentName arg0) {
                }
            };
            Intent intent = new Intent(context, FriendChatService.class);
            context.bindService(intent, connectionServiceFriendChatForStart, Context.BIND_NOT_FOREGROUND);
        }
    }

    public static void updateBadWordLibrary(final Context context) {


        Long latestUpdated = PesanSantunDB.getInstance(context).getlatestTimestampBadwords();

        Boolean isOkayToUpdate = true;
        if (latestUpdated != null) {
            Calendar lastUpdate = Calendar.getInstance();
            lastUpdate.setTimeInMillis(latestUpdated);
            lastUpdate.set(Calendar.MILLISECOND, 0);
            lastUpdate.set(Calendar.SECOND, 0);
            lastUpdate.set(Calendar.MINUTE, 0);
            lastUpdate.set(Calendar.HOUR, 0);

            Calendar today = Calendar.getInstance();
            today.setTimeInMillis(System.currentTimeMillis());
            today.set(Calendar.MILLISECOND, 0);
            today.set(Calendar.SECOND, 0);
            today.set(Calendar.MINUTE, 0);
            today.set(Calendar.HOUR, 0);

            if (lastUpdate.after(today))
                isOkayToUpdate = false;
        }
        if (isOkayToUpdate) {
            StorageReference mStorageRef;
            mStorageRef = FirebaseStorage.getInstance().getReference();
            StorageReference riversRef = mStorageRef.child("word_library/badwords.csv");


            try {
                final File localFile = File.createTempFile("badwords", ".csv");
                riversRef.getFile(localFile)
                        .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                                CSVReader csvReader = null;
                                try {
                                    csvReader = new CSVReader(new FileReader(localFile));
                                    String[] row = null;
                                    while ((row = csvReader.readNext()) != null) {

                                        for (String value : row) {
                                            String a = value;
                                            if (!value.isEmpty()) {
                                                Boolean isExist = PesanSantunDB.getInstance(context).checkBadWords(value.toLowerCase());

                                                if (!isExist) {
                                                    BadWord badWord = new BadWord();
                                                    badWord.setValue(value.toLowerCase());
                                                    badWord.setCreatedTimestamp(System.currentTimeMillis());
                                                    PesanSantunDB.getInstance(context).addBadWords(badWord);
                                                }
                                            }


                                        }


                                    }
//...
                                    csvReader.close();
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle failed download
                        // ...
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }


    public static void updateUserStatus(Context context) {
        if (isNetworkConnected(context)) {
            String uid = SharedPreferenceHelper.getInstance(context).getUID();
            if (!uid.equals("")) {
                FirebaseDatabase.getInstance().getReference().child("user/" + uid + "/status/timestamp").setValue(System.currentTimeMillis());
            }
        }
    }


    public static void updateFriendStatus(Context context, ListFriend listFriend) {
        if (isNetworkConnected(context)) {
            for (Friend friend : listFriend.getListFriend()) {
                final String fid = friend.id;
                FirebaseDatabase.getInstance().getReference().child("user/" + fid + "/status").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            HashMap mapStatus = (HashMap) dataSnapshot.getValue();
                            if ((boolean) mapStatus.get("isOnline") && (System.currentTimeMillis() - (long) mapStatus.get("timestamp")) > StaticConfig.TIME_TO_OFFLINE) {
                                FirebaseDatabase.getInstance().getReference().child("user/" + fid + "/status/isOnline").setValue(false);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }
    }

    public static boolean isNetworkConnected(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            return cm.getActiveNetworkInfo() != null;
        } catch (Exception e) {
            return true;
        }
    }

    public static Calendar formatLongToCalendar(long timestamp) {
        Calendar time = Calendar.getInstance();
        time.setTimeInMillis(timestamp);

        return time;
    }

    public static String checkBadWords(String textMsg, Context mContext) {
        String resultText = "";
        String[] split = textMsg.split(" ");

        HashMap specialMap = getSpecialCharacterLibrary();
        for (String key : split) {

            Pattern p = Pattern.compile("[^A-Za-z]");
            Matcher m = p.matcher(key);
            boolean isSpecial = m.find();

            if (key.equals("value")) {
                //do nothing
            } else if (isSpecial) {

                String[] splitKey = key.split("");
                String fixedKey = "";


                for (String alp : splitKey) {
                    fixedKey += specialMap.get(alp) != null ? specialMap.get(alp) : alp;
                }

                Boolean isExist = PesanSantunDB.getInstance(mContext).checkBadWords(fixedKey.toLowerCase());
                if (isExist) {
                    key = StringUtils.leftPad("", key.length(), "*");
                }

            } else {
                Boolean isExist = PesanSantunDB.getInstance(mContext).checkBadWords(key.toLowerCase());
                if (isExist) {
                    key = StringUtils.leftPad("", key.length(), "*");
                }
            }

            resultText += resultText.isEmpty() ? key : " " + key;


        }
        return resultText;
    }

    private static HashMap getSpecialCharacterLibrary() {
        HashMap specialMap = new HashMap();
        specialMap.put("4", "a");
        specialMap.put("@", "a");
        specialMap.put("α", "a");

        specialMap.put("8", "b");
        specialMap.put("6", "b");
        specialMap.put("β", "b");

        specialMap.put("©", "c");

        specialMap.put("3", "e");
        specialMap.put("€", "e");
        specialMap.put("£", "e");

        specialMap.put("9", "g");

        specialMap.put("1", "i");
        specialMap.put("!", "i");

        specialMap.put("7", "j");

        specialMap.put("0", "o");
        specialMap.put("◦", "o");

        specialMap.put("5", "s");
        specialMap.put("$", "s");

        return specialMap;
    }


    public static void playRingtone(Context context) {
        try {
            Uri sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.dropwater);

            RingtoneManager.setActualDefaultRingtoneUri(
                    context, RingtoneManager.TYPE_RINGTONE,
                    sound);
            Ringtone r = RingtoneManager.getRingtone(context, sound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

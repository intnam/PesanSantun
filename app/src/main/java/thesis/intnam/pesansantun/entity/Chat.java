package thesis.intnam.pesansantun.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Chat implements Parcelable {

    protected String key;
    protected String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Chat(String key, String value) {
        this.key = key;
        this.value = value;

    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.key);
        dest.writeString(this.value);

    }

    public Chat() {
    }

    protected Chat(Parcel in) {
        this.key = in.readString();
        this.value = in.readString();

    }

    public static final Creator<Chat> CREATOR = new Creator<Chat>() {
        @Override
        public Chat createFromParcel(Parcel source) {
            return new Chat(source);
        }

        @Override
        public Chat[] newArray(int size) {
            return new Chat[size];
        }

    };
}

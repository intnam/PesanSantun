package thesis.intnam.pesansantun;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import thesis.intnam.pesansantun.adapter.CommentItem;
import thesis.intnam.pesansantun.adapter.TimelineItem;
import thesis.intnam.pesansantun.data.PesanSantunDB;
import thesis.intnam.pesansantun.data.StaticConfig;
import thesis.intnam.pesansantun.entity.Comment;
import thesis.intnam.pesansantun.entity.Timeline;
import thesis.intnam.pesansantun.service.ServiceUtils;

public class CommentActivity extends AppCompatActivity {


    protected static FastItemAdapter<CommentItem> itemAdapter;
    @BindView(R.id.commentList)
    protected RecyclerView commentList;

    @BindView(R.id.commentText)
    protected EditText commentText;

    @BindView(R.id.totalComment)
    protected TextView totalComment;
    protected ArrayList<Comment> commentArrayList;
    private Context mContext;
    private String timelineId;
    private ChildEventListener commentListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.comment_page);
        ButterKnife.bind(this);
        mContext = this;
        itemAdapter = new FastItemAdapter<>();
        getSupportActionBar().hide();
        Intent intentData = getIntent();
        timelineId = intentData.getStringExtra("timelineId");


        displayCommentHistory();

    }

    private void displayCommentHistory() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        commentList.setLayoutManager(linearLayoutManager);
        commentList.setAdapter(itemAdapter);
        commentList.setNestedScrollingEnabled(true);

        commentListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                HashMap mapComment = (HashMap) dataSnapshot.getValue();
                Comment comment = new Comment();

                comment.setUserId((String) mapComment.get("userId"));
                comment.setCommentText((String) mapComment.get("commentText"));
                if (mapComment.get("timestamp") != null)
                    comment.setTimestamp((Long) mapComment.get("timestamp"));


                itemAdapter.add(new CommentItem(comment));
                itemAdapter.notifyDataSetChanged();
                totalComment.setText(itemAdapter.getAdapterItemCount() + " Comments ");

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        FirebaseDatabase.getInstance().getReference().child("comment/" + timelineId).addChildEventListener(commentListener);


    }

    @OnClick(R.id.btnSend)
    protected void sendComment() {

        if (validateInputText()) {
            final Comment newComment = new Comment();
            newComment.setUserId(StaticConfig.UID);
            newComment.setCommentText(commentText.getText().toString());
            newComment.setTimestamp(System.currentTimeMillis());

            commentText.setText("");
            commentText.setFocusable(false);
            commentText.setFocusableInTouchMode(true);

            FirebaseDatabase.getInstance().getReference().child("comment/" + timelineId).push().setValue(newComment).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(mContext,
                                getResources().getString(R.string.comment_successfully_posted),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }

    }

    private boolean validateInputText() {

        if (commentText.getText().toString().isEmpty()) {
            return false;
        } else {
            String resultText = ServiceUtils.checkBadWords(commentText.getText().toString(), mContext);

            if (resultText.contains("***")) {
                commentText.setText(resultText);
                Toast.makeText(mContext, getString(R.string.comment_cant_be_contained_badword), Toast.LENGTH_LONG).show();
                return false;
            } else {
                return true;
            }

        }
    }

    @Override
    public void onBackPressed() {
        FirebaseDatabase.getInstance().getReference().child("comment/" + timelineId).removeEventListener(commentListener);
        super.onBackPressed();
    }
}

package thesis.intnam.pesansantun.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import thesis.intnam.pesansantun.R;
import thesis.intnam.pesansantun.Utils.ImageUtils;
import thesis.intnam.pesansantun.data.StaticConfig;
import thesis.intnam.pesansantun.entity.Chat;
import thesis.intnam.pesansantun.entity.Friend;

import static thesis.intnam.pesansantun.service.ServiceUtils.formatLongToCalendar;

public class FriendListItem extends AbstractItem<FriendListItem, FriendListItem.ViewHolder> {

    public Friend friend;

    public FriendListItem(Friend friend) {
        this.friend = friend;
    }


    public FriendListItem() {
        this.friend = new Friend();
    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }


    @Override
    public int getType() {
        return 0;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.friendlist_item;
    }

    @Override
    public void bindView(ViewHolder holder, List payloads) {
        super.bindView(holder, payloads);
        Context mContext = holder.itemView.getContext();

        Bitmap avata;
        String avataString = friend.avata;
        if (!(StaticConfig.STR_DEFAULT_BASE64).equals(avataString)) {
            byte[] decodedString = Base64.decode(avataString, Base64.DEFAULT);
            avata = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        } else {
            avata = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.default_avata);
        }
        holder.icon_avata.setImageDrawable(ImageUtils.roundedImage(mContext, avata));

        holder.txtName.setText(friend.name);

        Calendar lastOnline = formatLongToCalendar((friend.status.timestamp));
        lastOnline.set(Calendar.MINUTE, lastOnline.get(Calendar.MINUTE) + 1);
        Calendar now = formatLongToCalendar(System.currentTimeMillis());

        if (lastOnline.after(now)) {
            holder.status.setText("Online");
            holder.status.setTextColor(mContext.getResources().getColor(R.color.green_text));
        } else {
            String time = new SimpleDateFormat("EEE, d MMM yyyy").format(new Date(friend.status.timestamp));
            String today = new SimpleDateFormat("EEE, d MMM yyyy").format(new Date(System.currentTimeMillis()));
            if (today.equals(time)) {
                holder.status.setText("Last seen on " + new SimpleDateFormat("HH:mm").format(new Date(friend.status.timestamp)));
            } else {
                holder.status.setText("Last seen on " + new SimpleDateFormat("d MMM").format(new Date(friend.status.timestamp)));
            }
            holder.status.setTextColor(mContext.getResources().getColor(R.color.grey_text));
        }

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.status)
        public TextView status;
        protected View view;
        @BindView(R.id.txtName)
        protected TextView txtName;
        @BindView(R.id.icon_avata)
        protected CircleImageView icon_avata;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;

        }
    }
}

package thesis.intnam.pesansantun;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChatDetailActivity extends AppCompatActivity {


    @OnClick(R.id.chat_listing)
    public void openChatDetail() {
        Intent intent = new Intent(this, ChatContentActivity.class);

        startActivity(intent);
    }


}

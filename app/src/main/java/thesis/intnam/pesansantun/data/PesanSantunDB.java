package thesis.intnam.pesansantun.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import thesis.intnam.pesansantun.adapter.ConversationItem;
import thesis.intnam.pesansantun.adapter.FriendListItem;
import thesis.intnam.pesansantun.entity.BadWord;
import thesis.intnam.pesansantun.entity.Friend;
import thesis.intnam.pesansantun.entity.ListFriend;
import thesis.intnam.pesansantun.entity.Message;
import thesis.intnam.pesansantun.entity.Timeline;

public final class PesanSantunDB {
    private static final String TEXT_TYPE = " TEXT";
    private static final String LONG_TYPE = " long";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_TABLE_FRIEND =
            "CREATE TABLE " + FeedFriend.TABLE_NAME + " (" +
                    FeedFriend.COLUMN_NAME_ID + " TEXT PRIMARY KEY," +
                    FeedFriend.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    FeedFriend.COLUMN_NAME_EMAIL + TEXT_TYPE + COMMA_SEP +
                    FeedFriend.COLUMN_NAME_ID_ROOM + TEXT_TYPE + COMMA_SEP +
                    FeedFriend.COLUMN_NAME_AVATA + TEXT_TYPE + COMMA_SEP +
                    FeedFriend.COLUMN_NAME_TIMESTAMP + LONG_TYPE +
                    " )";

    private static final String SQL_CREATE_TABLE_MESSAGE =
            "CREATE TABLE " + FeedMessage.TABLE_NAME + " (" +
                    FeedMessage.COLUMN_NAME_ID + " TEXT PRIMARY KEY," +
                    FeedMessage.COLUMN_NAME_ROOM_ID + TEXT_TYPE + COMMA_SEP +
                    FeedMessage.COLUMN_NAME_ID_SENDER + TEXT_TYPE + COMMA_SEP +
                    FeedMessage.COLUMN_NAME_ID_RECEIVER + TEXT_TYPE + COMMA_SEP +
                    FeedMessage.COLUMN_NAME_TEXT + TEXT_TYPE + COMMA_SEP +
                    FeedMessage.COLUMN_NAME_STATUS + TEXT_TYPE + COMMA_SEP +
                    FeedMessage.COLUMN_NAME_TIMESTAMP + LONG_TYPE + " )";


    private static final String SQL_CREATE_TABLE_BADWORDS =
            "CREATE TABLE " + FeedBadWords.TABLE_NAME + " (" +
                    FeedBadWords.COLUMN_NAME_VALUE + " TEXT PRIMARY KEY," +
                    FeedBadWords.COLUMN_NAME_CREATED_TIMESTAMP + LONG_TYPE + " )";

    private static final String SQL_CREATE_TABLE_TIMELINE =
            "CREATE TABLE " + FeedTimeline.TABLE_NAME + " (" +
                    FeedTimeline.COLUMN_NAME_ID + " TEXT PRIMARY KEY," +
                    FeedTimeline.COLUMN_NAME_USERID + TEXT_TYPE + COMMA_SEP +
                    FeedTimeline.COLUMN_NAME_TEXTSTATUS + TEXT_TYPE + COMMA_SEP +
                    FeedTimeline.COLUMN_NAME_CREATED_TIMESTAMP + LONG_TYPE +
                    " )";


    private static final String SQL_DELETE_TABLE_FRIEND =
            "DROP TABLE IF EXISTS " + FeedFriend.TABLE_NAME;

    private static final String SQL_DELETE_TABLE_MESSAGE =
            "DROP TABLE IF EXISTS " + FeedMessage.TABLE_NAME;

    private static final String SQL_DELETE_TABLE_BADWORDS =
            "DROP TABLE IF EXISTS " + FeedBadWords.TABLE_NAME;

    private static final String SQL_DELETE_TABLE_TIMELINE =
            "DROP TABLE IF EXISTS " + FeedTimeline.TABLE_NAME;

    private static PesanSantunDBHelper mDbHelper = null;
    private static PesanSantunDB instance = null;

    private PesanSantunDB() {
    }

    public static PesanSantunDB getInstance(Context context) {
        if (instance == null) {
            instance = new PesanSantunDB();
            mDbHelper = new PesanSantunDBHelper(context);
        }
        return instance;
    }

    public long addTimeline(Timeline timeline) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(FeedTimeline.COLUMN_NAME_ID, timeline.getId());
        values.put(FeedTimeline.COLUMN_NAME_USERID, timeline.getUserId());
        values.put(FeedTimeline.COLUMN_NAME_TEXTSTATUS, timeline.getTextStatus());
        values.put(FeedTimeline.COLUMN_NAME_CREATED_TIMESTAMP, timeline.getTimestamp());

        return db.insert(FeedTimeline.TABLE_NAME, null, values);
    }

    public ArrayList<Timeline> getTimelines() {
        ArrayList<Timeline> TimelineList = new ArrayList<>();
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        try {
            Cursor cursor = db.rawQuery("select * from " + FeedTimeline.TABLE_NAME
                            + " ORDER BY " + FeedTimeline.COLUMN_NAME_CREATED_TIMESTAMP + " DESC"
                    , null);
            while (cursor.moveToNext()) {
                Timeline timeline = new Timeline();
                timeline.setId(cursor.getString(0));
                timeline.setUserId(cursor.getString(1));
                timeline.setTextStatus(cursor.getString(2));
                timeline.setTimestamp(cursor.getLong(3));

                TimelineList.add(timeline);
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            return TimelineList;
        }
        return TimelineList;
    }

    public ArrayList<Timeline> getTimelinesByUserId(String UserId) {
        ArrayList<Timeline> TimelineList = new ArrayList<>();
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        try {
            Cursor cursor = db.rawQuery("select * from " + FeedTimeline.TABLE_NAME
                            + " where " + FeedTimeline.COLUMN_NAME_USERID + "= \"" + UserId + "\""
                            + " ORDER BY " + FeedTimeline.COLUMN_NAME_CREATED_TIMESTAMP + " DESC"
                    , null);
            while (cursor.moveToNext()) {
                Timeline timeline = new Timeline();
                timeline.setId(cursor.getString(0));
                timeline.setUserId(cursor.getString(1));
                timeline.setTextStatus(cursor.getString(2));
                timeline.setTimestamp(cursor.getLong(3));

                TimelineList.add(timeline);
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            return TimelineList;
        }
        return TimelineList;
    }


    public Boolean checkTimelineById(String id) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        Boolean isExist = false;
        try {
            Cursor cursor = db.rawQuery("select * from " + FeedTimeline.TABLE_NAME
                    + " where " + FeedTimeline.COLUMN_NAME_ID + "= \"" + id + "\"", null);
            while (cursor.moveToNext()) {
                isExist = true;
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return isExist;
    }

    public long addBadWords(BadWord badWord) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(FeedBadWords.COLUMN_NAME_VALUE, badWord.getValue());
        values.put(FeedBadWords.COLUMN_NAME_CREATED_TIMESTAMP, badWord.getCreatedTimestamp());
        // Insert the new row, returning the primary key value of the new row
        return db.insert(FeedBadWords.TABLE_NAME, null, values);
    }

    public Boolean checkBadWords(String value) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        Boolean isExist = false;
        try {
            Cursor cursor = db.rawQuery("select * from " + FeedBadWords.TABLE_NAME
                            + " where " + FeedBadWords.COLUMN_NAME_VALUE + "= \"" + value + "\""
                    , null);
            while (cursor.moveToNext()) {
                isExist = true;
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return isExist;
    }

    public Long getlatestTimestampBadwords() {

        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Long latestUpdate = null;
        String timestamp = "%%";
        try {
            Cursor cursor = db.rawQuery("select * from " + FeedBadWords.TABLE_NAME
                            + " ORDER BY " + FeedBadWords.COLUMN_NAME_CREATED_TIMESTAMP + " DESC LIMIT 1"
                    , null);
            while (cursor.moveToNext()) {
                latestUpdate = cursor.getLong(1);
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            return latestUpdate;
        }
        return latestUpdate;
    }


    public long addFriend(Friend friend) {


        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(FeedFriend.COLUMN_NAME_ID, friend.id);
        values.put(FeedFriend.COLUMN_NAME_NAME, friend.name);
        values.put(FeedFriend.COLUMN_NAME_EMAIL, friend.email);
        values.put(FeedFriend.COLUMN_NAME_ID_ROOM, friend.idRoom);
        values.put(FeedFriend.COLUMN_NAME_AVATA, friend.avata);
        values.put(FeedFriend.COLUMN_NAME_TIMESTAMP, friend.status.timestamp);
        // Insert the new row, returning the primary key value of the new row
        return db.insert(FeedFriend.TABLE_NAME, null, values);
    }


    public Friend getFriendById(String id) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Friend friend = null;

        try {
            Cursor cursor = db.rawQuery("select * from " + FeedFriend.TABLE_NAME
                    + " where " + FeedFriend.COLUMN_NAME_ID + "= \"" + id + "\"", null);
            while (cursor.moveToNext()) {
                friend = new Friend();
                friend.id = cursor.getString(0);
                friend.name = cursor.getString(1);
                friend.email = cursor.getString(2);
                friend.idRoom = cursor.getString(3);
                friend.avata = cursor.getString(4);
                friend.status.timestamp = cursor.getLong(5);

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return friend;
    }

    public Friend getFriendByIdRoom(String idRoom) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Friend friend = null;

        try {
            Cursor cursor = db.rawQuery("select * from " + FeedFriend.TABLE_NAME
                    + " where " + FeedFriend.COLUMN_NAME_ID_ROOM + "= \"" + idRoom + "\"", null);
            while (cursor.moveToNext()) {
                friend = new Friend();
                friend.id = cursor.getString(0);
                friend.name = cursor.getString(1);
                friend.email = cursor.getString(2);
                friend.idRoom = cursor.getString(3);
                friend.avata = cursor.getString(4);
                friend.status.timestamp = cursor.getLong(5);

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return friend;
    }

    public Boolean getFriendByEmail(String email) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Friend friend = null;
        Boolean isExist = false;
        try {
            Cursor cursor = db.rawQuery("select * from " + FeedFriend.TABLE_NAME
                    + " where " + FeedFriend.COLUMN_NAME_EMAIL + "= \"" + email + "\"", null);
            while (cursor.moveToNext()) {
                isExist = true;
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return isExist;
    }

    public void saveFriendInfo(Friend friend) {

        Friend existingFriend = getFriendById(friend.id);
        if (existingFriend == null)
            addFriend(friend);
        else {
            SQLiteDatabase db = mDbHelper.getWritableDatabase();
            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(FeedFriend.COLUMN_NAME_NAME, friend.name);
            values.put(FeedFriend.COLUMN_NAME_AVATA, friend.avata);

            String queryCondition = FeedFriend.COLUMN_NAME_ID + "= \"" + friend.id + "\"";
            db.update(FeedFriend.TABLE_NAME, values, queryCondition, null);
        }
    }


    public long updateFriendStatusInfo(Friend friend) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(FeedFriend.COLUMN_NAME_TIMESTAMP, friend.status.timestamp);

        String queryCondition = FeedFriend.COLUMN_NAME_ID + "= \"" + friend.id + "\"";
        return db.update(FeedFriend.TABLE_NAME, values, queryCondition, null);
    }

    public void addListFriend(ListFriend listFriend) {
        for (Friend friend : listFriend.getListFriend()) {
            addFriend(friend);
        }
    }

    public ArrayList<Friend> getAllFriendList() {
        ArrayList<Friend> friendList = new ArrayList<>();
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        try {
            Cursor cursor = db.rawQuery("select * from " + FeedFriend.TABLE_NAME
                            + " ORDER BY " + FeedFriend.COLUMN_NAME_NAME + " ASC"
                    , null);
            while (cursor.moveToNext()) {
                Friend friend = new Friend();
                friend.id = cursor.getString(0);
                friend.name = cursor.getString(1);
                friend.email = cursor.getString(2);
                friend.idRoom = cursor.getString(3);
                friend.avata = cursor.getString(4);
                friend.status.timestamp = cursor.getLong(5);

                friendList.add(friend);
            }
            cursor.close();
        } catch (Exception e) {
            return friendList;
        }
        return friendList;
    }

    public ArrayList<FriendListItem> getAllFriendListItem() {
        ArrayList<FriendListItem> friendList = new ArrayList<>();
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        try {
            Cursor cursor = db.rawQuery("select * from " + FeedFriend.TABLE_NAME
                    + " ORDER BY " + FeedFriend.COLUMN_NAME_NAME + " ASC", null);
            while (cursor.moveToNext()) {
                Friend friend = new Friend();
                friend.id = cursor.getString(0);
                friend.name = cursor.getString(1);
                friend.email = cursor.getString(2);
                friend.idRoom = cursor.getString(3);
                friend.avata = cursor.getString(4);
                friend.status.timestamp = cursor.getLong(5);


                FriendListItem item = new FriendListItem(friend);
                friendList.add(item);
            }
            cursor.close();
        } catch (Exception e) {
            return friendList;
        }
        return friendList;
    }

    public ListFriend getListFriendAll() {
        ListFriend listFriend = new ListFriend();
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        try {
            Cursor cursor = db.rawQuery("select * from " + FeedFriend.TABLE_NAME, null);
            while (cursor.moveToNext()) {
                Friend friend = new Friend();
                friend.id = cursor.getString(0);
                friend.name = cursor.getString(1);
                friend.email = cursor.getString(2);
                friend.idRoom = cursor.getString(3);
                friend.avata = cursor.getString(4);
                friend.status.timestamp = cursor.getLong(5);

                listFriend.getListFriend().add(friend);
            }
            cursor.close();
        } catch (Exception e) {
            return new ListFriend();
        }
        return listFriend;
    }


    public ListFriend getListFriendByMessage() {
        ListFriend listFriend = new ListFriend();
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        try {
            Cursor cursor = db.rawQuery("select * from " + FeedFriend.TABLE_NAME
                    + "  INNER JOIN " + FeedMessage.TABLE_NAME
                    + " ON " + FeedFriend.TABLE_NAME + "." + FeedFriend.COLUMN_NAME_ID_ROOM
                    + " = " + FeedMessage.TABLE_NAME + "." + FeedMessage.COLUMN_NAME_ROOM_ID
                    + " GROUP BY " + FeedFriend.TABLE_NAME + "." + FeedFriend.COLUMN_NAME_ID_ROOM
                    + " ORDER BY " + FeedMessage.TABLE_NAME + "." + FeedMessage.COLUMN_NAME_TIMESTAMP + " DESC", null);
            while (cursor.moveToNext()) {
                Friend friend = new Friend();
                friend.id = cursor.getString(0);
                friend.name = cursor.getString(1);
                friend.email = cursor.getString(2);
                friend.idRoom = cursor.getString(3);
                friend.avata = cursor.getString(4);
                friend.status.timestamp = cursor.getLong(5);

                listFriend.getListFriend().add(friend);
            }
            cursor.close();
        } catch (Exception e) {
            return new ListFriend();
        }
        return listFriend;
    }


    public long addMessage(Message message) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(FeedMessage.COLUMN_NAME_ID, message.id);
        values.put(FeedMessage.COLUMN_NAME_ROOM_ID, message.roomId);
        values.put(FeedMessage.COLUMN_NAME_ID_SENDER, message.idSender);
        values.put(FeedMessage.COLUMN_NAME_ID_RECEIVER, message.idReceiver);
        values.put(FeedMessage.COLUMN_NAME_TEXT, message.text);
        values.put(FeedMessage.COLUMN_NAME_STATUS, message.status);
        values.put(FeedMessage.COLUMN_NAME_TIMESTAMP, message.timestamp);
        // Insert the new row, returning the primary key value of the new row
        return db.insert(FeedMessage.TABLE_NAME, null, values);
    }


    public Boolean checkMessageById(String id) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        Boolean isExist = false;
        try {
            Cursor cursor = db.rawQuery("select * from " + FeedMessage.TABLE_NAME
                    + " where " + FeedTimeline.COLUMN_NAME_ID + "= \"" + id + "\"", null);
            while (cursor.moveToNext()) {
                isExist = true;
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return isExist;
    }

    public long updateMessage(Message message) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(FeedMessage.COLUMN_NAME_ID, message.id);
        values.put(FeedMessage.COLUMN_NAME_STATUS, message.status);

        String queryCondition = FeedMessage.COLUMN_NAME_ID + "= \"" + message.id + "\"";
        return db.update(FeedMessage.TABLE_NAME, values, queryCondition, null);
    }

    public void deleteChatFromDevicesByRoomId(String roomId) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        db.delete(FeedMessage.TABLE_NAME, FeedMessage.COLUMN_NAME_ROOM_ID + "= \"" + roomId + "\"", null);
    }

    public void deleteChatFromDevicesByMsgId(String msgId) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        db.delete(FeedMessage.TABLE_NAME, FeedMessage.COLUMN_NAME_ID + "= \"" + msgId + "\"", null);
    }

    public ArrayList<ConversationItem> getListMessageByRoomId(String roomId) {
        ArrayList<ConversationItem> conversationItemList = new ArrayList<>();
        ConversationItem conversationItem;
        String displayedDate = null;
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        try {
            Cursor cursor = db.rawQuery("select * from " + FeedMessage.TABLE_NAME
                    + " where " + FeedMessage.COLUMN_NAME_ROOM_ID + "= \"" + roomId + "\"", null);
            while (cursor.moveToNext()) {
                Message message = new Message();
                message.id = cursor.getString(0);
                message.roomId = cursor.getString(1);
                message.idSender = cursor.getString(2);
                message.idReceiver = cursor.getString(3);
                message.text = cursor.getString(4);
                message.status = cursor.getString(5);
                message.timestamp = cursor.getLong(6);

                String recordedDate = new SimpleDateFormat("EEE, d MMM yyyy").format(new Date(message.timestamp));

                if (displayedDate == null) {
                    message.isDisplayedTime = true;
                    displayedDate = recordedDate;
                } else {
                    if (!recordedDate.equals(displayedDate)) {
                        message.isDisplayedTime = true;
                        displayedDate = recordedDate;
                    } else {
                        message.isDisplayedTime = false;
                    }
                }

                conversationItem = new ConversationItem(message);
                conversationItemList.add(conversationItem);
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            return conversationItemList;
        }
        return conversationItemList;
    }


    public Message getLatestMessageByRoomId(String roomId) {
        Message message = null;
        String displayedDate = null;
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        try {
            Cursor cursor = db.rawQuery("select * from " + FeedMessage.TABLE_NAME
                    + " where " + FeedMessage.COLUMN_NAME_ROOM_ID + "= \"" + roomId + "\""
                    + " ORDER BY " + FeedMessage.COLUMN_NAME_TIMESTAMP
                    + " DESC LIMIT 1", null);
            while (cursor.moveToNext()) {
                message = new Message();
                message.id = cursor.getString(0);
                message.roomId = cursor.getString(1);
                message.idSender = cursor.getString(2);
                message.idReceiver = cursor.getString(3);
                message.text = cursor.getString(4);
                message.status = cursor.getString(5);
                message.timestamp = cursor.getLong(6);

                String recordedDate = new SimpleDateFormat("EEE, d MMM yyyy").format(new Date(message.timestamp));

                if (displayedDate == null) {
                    message.isDisplayedTime = true;
                    displayedDate = recordedDate;
                } else {
                    if (!recordedDate.equals(displayedDate)) {
                        message.isDisplayedTime = true;
                        displayedDate = recordedDate;
                    } else {
                        message.isDisplayedTime = false;
                    }
                }

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            return message;
        }
        return message;
    }

    public ArrayList<Message> getLatestMessageFromAllFriends() {
        ArrayList<Message> messageList = new ArrayList<>();
        String displayedDate = null;
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        try {
            Cursor cursor = db.rawQuery("select * from " + FeedMessage.TABLE_NAME
//                    + " where " + FeedMessage.COLUMN_NAME_ROOM_ID + "= \"" + roomId + "\""
                    + " GROUP BY " + FeedMessage.TABLE_NAME + "." + FeedMessage.COLUMN_NAME_ROOM_ID
                    + " ORDER BY " + FeedMessage.COLUMN_NAME_TIMESTAMP
                    + " DESC", null);
            while (cursor.moveToNext()) {
                Message message = new Message();
                message.id = cursor.getString(0);
                message.roomId = cursor.getString(1);
                message.idSender = cursor.getString(2);
                message.idReceiver = cursor.getString(3);
                message.text = cursor.getString(4);
                message.status = cursor.getString(5);
                message.timestamp = cursor.getLong(6);

                String recordedDate = new SimpleDateFormat("EEE, d MMM yyyy").format(new Date(message.timestamp));

                if (displayedDate == null) {
                    message.isDisplayedTime = true;
                    displayedDate = recordedDate;
                } else {
                    if (!recordedDate.equals(displayedDate)) {
                        message.isDisplayedTime = true;
                        displayedDate = recordedDate;
                    } else {
                        message.isDisplayedTime = false;
                    }
                }

                messageList.add(message);
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            return messageList;
        }
        return messageList;
    }

    public void dropDB() {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        db.execSQL(SQL_DELETE_TABLE_FRIEND);
        db.execSQL(SQL_DELETE_TABLE_MESSAGE);
        db.execSQL(SQL_DELETE_TABLE_BADWORDS);
        db.execSQL(SQL_DELETE_TABLE_TIMELINE);

        db.execSQL(SQL_CREATE_TABLE_FRIEND);
        db.execSQL(SQL_CREATE_TABLE_MESSAGE);
        db.execSQL(SQL_CREATE_TABLE_BADWORDS);
        db.execSQL(SQL_CREATE_TABLE_TIMELINE);

    }


    /* Inner class that defines the table contents */
    public static class FeedFriend implements BaseColumns {
        static final String TABLE_NAME = "friend";
        static final String COLUMN_NAME_ID = "friendID";
        static final String COLUMN_NAME_NAME = "name";
        static final String COLUMN_NAME_EMAIL = "email";
        static final String COLUMN_NAME_ID_ROOM = "idRoom";
        static final String COLUMN_NAME_AVATA = "avata";
        static final String COLUMN_NAME_TIMESTAMP = "timestamp";
    }


    /* Inner class that defines the table contents */
    public static class FeedMessage implements BaseColumns {
        static final String TABLE_NAME = "message";
        static final String COLUMN_NAME_ID = "id";
        static final String COLUMN_NAME_ROOM_ID = "roomId";
        static final String COLUMN_NAME_ID_SENDER = "idSender";
        static final String COLUMN_NAME_ID_RECEIVER = "idReceiver";
        static final String COLUMN_NAME_TEXT = "text";
        static final String COLUMN_NAME_STATUS = "status";
        static final String COLUMN_NAME_TIMESTAMP = "timestamp";
    }

    public static class FeedBadWords implements BaseColumns {
        static final String TABLE_NAME = "badwords";
        static final String COLUMN_NAME_VALUE = "value";
        static final String COLUMN_NAME_CREATED_TIMESTAMP = "created_timestamp";
    }

    public static class FeedTimeline implements BaseColumns {
        static final String TABLE_NAME = "timeline";
        static final String COLUMN_NAME_ID = "id";
        static final String COLUMN_NAME_USERID = "userid";
        static final String COLUMN_NAME_TEXTSTATUS = "textstatus";
        static final String COLUMN_NAME_CREATED_TIMESTAMP = "created_timestamp";
    }


    private static class PesanSantunDBHelper extends SQLiteOpenHelper {
        // If you change the database schema, you must increment the database version.
        static final int DATABASE_VERSION = 2;
        static final String DATABASE_NAME = "PesanSantun.db";

        PesanSantunDBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_TABLE_FRIEND);
            db.execSQL(SQL_CREATE_TABLE_MESSAGE);
            db.execSQL(SQL_CREATE_TABLE_BADWORDS);
            db.execSQL(SQL_CREATE_TABLE_TIMELINE);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // This database is only a cache for online data, so its upgrade policy is
            // to simply to discard the data and start over
            db.execSQL(SQL_DELETE_TABLE_FRIEND);
            db.execSQL(SQL_DELETE_TABLE_MESSAGE);
            db.execSQL(SQL_DELETE_TABLE_BADWORDS);
            db.execSQL(SQL_DELETE_TABLE_TIMELINE);
            onCreate(db);
        }

        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }
    }
}

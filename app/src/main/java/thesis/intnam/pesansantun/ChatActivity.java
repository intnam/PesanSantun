package thesis.intnam.pesansantun;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import thesis.intnam.pesansantun.adapter.ConversationItem;
import thesis.intnam.pesansantun.data.PesanSantunDB;
import thesis.intnam.pesansantun.data.StaticConfig;
import thesis.intnam.pesansantun.entity.Conversation;
import thesis.intnam.pesansantun.entity.Message;
import thesis.intnam.pesansantun.fragment.ChatFragment;
import thesis.intnam.pesansantun.service.ServiceUtils;


public class ChatActivity extends AppCompatActivity implements View.OnClickListener {
    public static final int VIEW_TYPE_USER_MESSAGE = 0;
    public static final int VIEW_TYPE_FRIEND_MESSAGE = 1;
    public static HashMap<String, Bitmap> bitmapAvataFriend;
    private static String roomId, friendName;
    private static LinearLayoutManager linearLayoutManager;
    private static FastItemAdapter<ConversationItem> fastItemAdapter;
    private static Context mContext;
    public Bitmap userAvata, friendAvata;
    @BindView(R.id.editWriteMessage)
    protected EditText editWriteMessage;
    private RecyclerView recyclerChat;
    private ArrayList<CharSequence> idFriend;
    private Conversation conversation;
    private ArrayList<ConversationItem> conversationItemList;
    private ImageButton btnSend;
    private String displayedDate;
    private ChildEventListener eventListener;

    private static void updateMessageStatusById(Message message) {
        PesanSantunDB.getInstance(mContext).updateMessage(message);
    }

    private static void deleteReadMessagefromServer(String messageKey) {
        FirebaseDatabase.getInstance().getReference().child("message")
                .child(roomId).child(messageKey).removeValue();
    }

    public static void addDetailMessage(Message newMessage) {

        String recordedDate = new SimpleDateFormat("EEE, d MMM yyyy").format(new Date(newMessage.timestamp));

        Boolean isDisplayedTime = true;
        int countDisplayMessageList = fastItemAdapter.getAdapterItems().isEmpty() ? fastItemAdapter.getAdapterItems().size() - 1 : 0;
        for (int i = countDisplayMessageList; i >= 0; i--) {
            ConversationItem item = fastItemAdapter.getAdapterItem(i);
            String lastDate = new SimpleDateFormat("EEE, d MMM yyyy").format(new Date(newMessage.timestamp));
            String displayedDate = new SimpleDateFormat("EEE, d MMM yyyy").format(new Date(newMessage.timestamp));


            if (item.message.isDisplayedTime) {
                isDisplayedTime = recordedDate.equals(displayedDate) ? false : true;
                break;
            }
        }
        newMessage.isDisplayedTime = isDisplayedTime;

        fastItemAdapter.add(new ConversationItem(newMessage));
        linearLayoutManager.scrollToPosition(fastItemAdapter.getAdapterItems().size() - 1);
        fastItemAdapter.notifyDataSetChanged();
    }

    public static void updateDetailMessage(Message message) {

        if (message.idSender.equals(StaticConfig.UID)) {

            for (ConversationItem item : fastItemAdapter.getAdapterItems()) {
                Message updatedMessage = item.message;

                if (message.id.equals(updatedMessage.id)) {
                    updatedMessage.status = message.status;

                    if ("read".equals(message.status)) {
                        updateMessageStatusById(message);
//                        deleteReadMessagefromServer(message.id);
                    } else if ("blocked".equals(message.status)) {
                        PesanSantunDB.getInstance(mContext).deleteChatFromDevicesByMsgId(message.id);
                        deleteReadMessagefromServer(message.id);
                        updatedMessage.text = message.text;
                        Toast.makeText(mContext, mContext.getString(R.string.popup_block_badword_message), Toast.LENGTH_LONG).show();
                    }
                    break;
                }

            }

            linearLayoutManager.scrollToPosition(fastItemAdapter.getAdapterItems().size() - 1);
            fastItemAdapter.notifyDataSetChanged();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        mContext = this;
        Intent intentData = getIntent();
        conversationItemList = new ArrayList<>();
        fastItemAdapter = new FastItemAdapter<>();
        idFriend = intentData.getCharSequenceArrayListExtra(StaticConfig.INTENT_KEY_CHAT_ID);
        roomId = intentData.getStringExtra(StaticConfig.INTENT_KEY_CHAT_ROOM_ID);
        friendName = intentData.getStringExtra(StaticConfig.INTENT_KEY_CHAT_FRIEND);

        conversation = new Conversation();
        btnSend = (ImageButton) findViewById(R.id.btnSend);
        btnSend.setOnClickListener(this);

        editWriteMessage.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    btnSend.performClick();
                    handled = true;
                }
                return handled;
            }
        });

        setIsOnDetailPage(roomId);
        setMessages();
    }

    private void setIsOnDetailPage(String roomId) {
        ChatFragment.setIsOnDetailPage(roomId);
    }

    private void setMessages() {
        getSupportActionBar().setTitle(friendName);

        conversationItemList = PesanSantunDB.getInstance(mContext).getListMessageByRoomId(roomId);

        for (ConversationItem item : conversationItemList) {
            Message message = item.message;
            if (message.status == null || message.status.equals("sent")) {
                if (!message.idSender.equals(StaticConfig.UID)) {
                    message.status = "read";
                    updateMessageStatusById(message);
                    setMessageStatus(roomId + "/" + message.id, "read");
                }

            }
        }


        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerChat = (RecyclerView) findViewById(R.id.recyclerChat);
        recyclerChat.setLayoutManager(linearLayoutManager);
        fastItemAdapter.add(conversationItemList);
        linearLayoutManager.scrollToPosition(fastItemAdapter.getAdapterItems().size() - 1);
        fastItemAdapter.notifyDataSetChanged();
        recyclerChat.setAdapter(fastItemAdapter);
    }

    private void sendMsgBlockedInfo(Message newMessage) {

    }

    private void saveMessageIntoDevice(Message newMessage) {
        PesanSantunDB.getInstance(mContext).addMessage(newMessage);
    }

    private void setMessageStatus(String messageKey, String status) {

        FirebaseDatabase.getInstance().getReference().child("message/" + messageKey + "/status").setValue(status);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent result = new Intent();
            result.putExtra("idFriend", idFriend.get(0));
            setResult(RESULT_OK, result);
            this.finish();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
//        FirebaseDatabase.getInstance().getReference().child("message/" + roomId).removeEventListener(eventListener);
        Intent result = new Intent();
        result.putExtra("idFriend", idFriend.get(0));
        setResult(RESULT_OK, result);
        this.finish();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnSend) {
            String content = editWriteMessage.getText().toString().trim();
            if (content.length() > 0) {

                editWriteMessage.setText("");
                Message newMessage = new Message();
                newMessage.text = content;
                newMessage.idSender = StaticConfig.UID;
                newMessage.idReceiver = roomId;
                newMessage.timestamp = System.currentTimeMillis();
                newMessage.status = "sent";

                String textMsg = content;

                String resultText = ServiceUtils.checkBadWords(textMsg, mContext);

                if (resultText.contains("***")) {
                    Toast.makeText(mContext, getString(R.string.popup_block_badword_message), Toast.LENGTH_LONG).show();
                    newMessage.text = resultText;
                    newMessage.status = "blocked";

                    String recordedDate = new SimpleDateFormat("EEE, d MMM yyyy").format(new Date(newMessage.timestamp));

                    if (displayedDate == null) {
                        newMessage.isDisplayedTime = true;
                        displayedDate = recordedDate;
                    } else {
                        if (!recordedDate.equals(displayedDate)) {
                            newMessage.isDisplayedTime = true;
                            displayedDate = recordedDate;
                        } else {
                            newMessage.isDisplayedTime = false;
                        }
                    }
                    fastItemAdapter.add(new ConversationItem(newMessage));
                    linearLayoutManager.scrollToPosition(fastItemAdapter.getAdapterItems().size() - 1);
                    fastItemAdapter.notifyDataSetChanged();

                } else {
                    saveMessageIntoFirebase(newMessage);
                }
            }
        }
    }

    private void saveMessageIntoFirebase(final Message newMessage) {
        FirebaseDatabase.getInstance().getReference().child("message/" + roomId).push().setValue(newMessage)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful())
                            saveMessageIntoDevice(newMessage);
                    }
                });
    }
}



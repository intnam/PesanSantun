package thesis.intnam.pesansantun.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import thesis.intnam.pesansantun.CommentActivity;
import thesis.intnam.pesansantun.R;
import thesis.intnam.pesansantun.Utils.ImageUtils;
import thesis.intnam.pesansantun.data.PesanSantunDB;
import thesis.intnam.pesansantun.data.SharedPreferenceHelper;
import thesis.intnam.pesansantun.data.StaticConfig;
import thesis.intnam.pesansantun.entity.Comment;
import thesis.intnam.pesansantun.entity.Friend;
import thesis.intnam.pesansantun.entity.Timeline;
import thesis.intnam.pesansantun.entity.User;

public class CommentItem extends AbstractItem<CommentItem, CommentItem.ViewHolder> {

    public Comment comment;

    public CommentItem(Comment comment) {
        this.comment = comment;
    }


    public CommentItem() {
        this.comment = new Comment();
    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }


    @Override
    public int getType() {
        return 0;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.comment_item;
    }

    @Override
    public void bindView(ViewHolder holder, List payloads) {
        super.bindView(holder, payloads);
        final Context ctx = holder.itemView.getContext();
        String avataString = "";
        Bitmap avata;

        if (StaticConfig.UID.equals(comment.getUserId())) {
            SharedPreferenceHelper prefHelper = SharedPreferenceHelper.getInstance(ctx);
            User myAccount = prefHelper.getUserInfo();
            avataString = myAccount.avata;
            holder.user_name.setText(myAccount.name);

        } else {
            Friend friend = PesanSantunDB.getInstance(ctx).getFriendById(comment.getUserId());

            if (friend != null) {
                avataString = friend.avata;
                holder.user_name.setText(friend.name);
            } else {
                avataString = StaticConfig.STR_DEFAULT_BASE64;
            }

        }
        if (!(StaticConfig.STR_DEFAULT_BASE64).equals(avataString)) {
            byte[] decodedString = Base64.decode(avataString, Base64.DEFAULT);
            avata = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        } else {
            avata = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.default_avata);
        }

        holder.profile_picture.setImageDrawable(ImageUtils.roundedImage(ctx, avata));
        holder.createdTime.setText(new SimpleDateFormat("d MMM yyyy HH:mm")
                .format(new Date(comment.getTimestamp())));
        holder.commentText.setText(comment.getCommentText());


    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

        @BindView(R.id.profile_picture)
        protected CircleImageView profile_picture;

        @BindView(R.id.user_name)
        protected TextView user_name;

        @BindView(R.id.createdTime)
        protected TextView createdTime;

        @BindView(R.id.commentText)
        protected TextView commentText;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;

        }
    }
}

package thesis.intnam.pesansantun;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.HashMap;

import thesis.intnam.pesansantun.data.SharedPreferenceHelper;
import thesis.intnam.pesansantun.entity.User;

public class SplashScreenActivity extends AppCompatActivity {
    private static final String TAG = "start_page";
    // Splash screen timer
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static int SPLASH_TIME_OUT = 500;
    HashMap<String, String> user;
    private FirebaseAuth mAuth;
    private Context mContext;
    private User myAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        getSupportActionBar().hide();
        mContext = this;
        mAuth = FirebaseAuth.getInstance();
        final FirebaseUser user = mAuth.getCurrentUser();

        SharedPreferenceHelper prefHelper = SharedPreferenceHelper.getInstance(mContext);
        myAccount = prefHelper.getUserInfo();
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {

                if (checkPlayServices()) {
                    Intent intent;
                    if (user != null) {

//                         User is signed in
                        Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                        if (myAccount == null) {
                            intent = new Intent(mContext, ProfileInfoActivity.class);
                        } else {
                            intent = new Intent(mContext, MainActivity.class);
                        }

                    } else {
                        // User is signed out
                        Log.d(TAG, "onAuthStateChanged:signed_out");
                        intent = new Intent(SplashScreenActivity.this, LoginActivity.class);

                    }
                    finish();
                    startActivity(intent);
                }

            }
        }, SPLASH_TIME_OUT);


    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported, please update your Google Play Service");
                finish();
            }
            return false;
        }
        return true;
    }
}

package thesis.intnam.pesansantun.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import thesis.intnam.pesansantun.R;
import thesis.intnam.pesansantun.Utils.ImageUtils;
import thesis.intnam.pesansantun.data.StaticConfig;
import thesis.intnam.pesansantun.entity.ChatList;

public class ChatListItem extends AbstractItem<ChatListItem, ChatListItem.ViewHolder> {

    public ChatList chatList;
    protected Context mContext;

    public ChatListItem(ChatList chatList) {
        this.chatList = chatList;
    }


    public ChatListItem() {
        this.chatList = new ChatList();
    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }


    @Override
    public int getType() {
        return 0;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.chatlist_item;
    }

    @Override
    public void bindView(final ViewHolder holder, List payloads) {
        super.bindView(holder, payloads);
        mContext = holder.itemView.getContext();
        String avataString = chatList.getAvata();
        Bitmap avata;
        if (!(StaticConfig.STR_DEFAULT_BASE64).equals(avataString)) {
            byte[] decodedString = Base64.decode(avataString, Base64.DEFAULT);
            avata = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        } else {
            avata = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.default_avata);
        }
        holder.icon_avata.setImageDrawable(ImageUtils.roundedImage(mContext, avata));

        holder.txtName.setText(chatList.getName());
        holder.txtMessage.setText(chatList.getChatText());

        if (!StaticConfig.UID.equals(chatList.getSenderId())) {
            if (("sent").equals(chatList.getMessageStatus()) || chatList.getMessageStatus() == null)
                holder.txtMessage.setTypeface(Typeface.DEFAULT_BOLD);
            else
                holder.txtMessage.setTypeface(Typeface.DEFAULT);
        } else
            holder.txtMessage.setTypeface(Typeface.DEFAULT);

        holder.txtTime.setText(chatList.getTxtTime());

        holder.chatItem_layout.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        holder.deleteChat_layout.setVisibility(View.GONE);
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

        @BindView(R.id.txtName)
        protected TextView txtName;

        @BindView(R.id.icon_avata)
        protected CircleImageView icon_avata;

        @BindView(R.id.txtMessage)
        protected TextView txtMessage;

        @BindView(R.id.txtTime)
        protected TextView txtTime;

        @BindView(R.id.chatItem_layout)
        protected LinearLayout chatItem_layout;

        @BindView(R.id.deleteChat_layout)
        protected LinearLayout deleteChat_layout;



        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;


        }
    }
}

package thesis.intnam.pesansantun.fragment;


import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.itemanimators.SlideDownAlphaAnimator;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import thesis.intnam.pesansantun.R;
import thesis.intnam.pesansantun.SettingDetailActivity;
import thesis.intnam.pesansantun.Utils.ImageUtils;
import thesis.intnam.pesansantun.adapter.TimelineItem;
import thesis.intnam.pesansantun.data.PesanSantunDB;
import thesis.intnam.pesansantun.data.SharedPreferenceHelper;
import thesis.intnam.pesansantun.data.StaticConfig;
import thesis.intnam.pesansantun.entity.Timeline;
import thesis.intnam.pesansantun.entity.User;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    private static final int REQUEST_PROFILE_SETTINGS = 1;
    protected static ArrayList<Timeline> timelineArrayList;
    public RecyclerView userPostList;
    protected LinearLayout id_post;
    protected LinearLayout id_setting;
    protected CircleImageView profile_picture;
    protected TextView displayName, postLabel, settingLabel;
    protected FastItemAdapter<TimelineItem> itemAdapter;
    LovelyProgressDialog dialogWait;
    private User myAccount;
    private LinearLayoutManager linearLayoutManager;
    private Context mContext;

    public static void onrefresh() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.profile_page, container, false);
        mContext = view.getContext();
        profile_picture = (CircleImageView) view.findViewById(R.id.profile_picture);
        displayName = (TextView) view.findViewById(R.id.displayName);
        postLabel = (TextView) view.findViewById(R.id.postLabel);
        settingLabel = (TextView) view.findViewById(R.id.settingLabel);
        id_post = (LinearLayout) view.findViewById(R.id.id_post);
        id_setting = (LinearLayout) view.findViewById(R.id.id_setting);
        userPostList = (RecyclerView) view.findViewById(R.id.userPostList);

        setUserPostList();
        retrieveDataFromFirebaseDB();

        id_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setIsSettingTabClicked(true);
                Intent intent = new Intent(getActivity(), SettingDetailActivity.class);
                startActivityForResult(intent, REQUEST_PROFILE_SETTINGS);
            }
        });

        return view;
    }


    private void retrieveDataFromFirebaseDB() {

        showDialog();
        FirebaseDatabase.getInstance().getReference().child("user").child(StaticConfig.UID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                myAccount = dataSnapshot.getValue(User.class);
                if (myAccount != null) {
                    displayName.setText(myAccount.name);
                    setImageAvatar(mContext, myAccount.avata);
                    SharedPreferenceHelper preferenceHelper = SharedPreferenceHelper.getInstance(mContext);
                    preferenceHelper.saveUserInfo(myAccount);
                    hideDialog();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setUserPostList() {
        itemAdapter = new FastItemAdapter<>();
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        timelineArrayList = PesanSantunDB.getInstance(getActivity()).getTimelinesByUserId(StaticConfig.UID);


        for (Timeline timeline : timelineArrayList) {
            itemAdapter.add(new TimelineItem(timeline));
        }
        postLabel.setText(timelineArrayList.size() + " Posts");
        userPostList.setLayoutManager(linearLayoutManager);
        userPostList.setAdapter(itemAdapter);
        userPostList.setNestedScrollingEnabled(true);
    }

    @Override
    public void onResume() {
        setUserPostList();
        super.onResume();
    }

    private void setIsSettingTabClicked(boolean isSettingCliked) {
        if (isSettingCliked) {
            id_post.setBackgroundColor(getResources().getColor(R.color.grey_background));
            id_setting.setBackgroundColor(getResources().getColor(R.color.primary));
            settingLabel.setTextColor(getResources().getColor(R.color.white));
            postLabel.setTextColor(getResources().getColor(R.color.black));
        } else {
            id_setting.setBackgroundColor(getResources().getColor(R.color.grey_background));
            id_post.setBackgroundColor(getResources().getColor(R.color.primary));
            postLabel.setTextColor(getResources().getColor(R.color.white));
            settingLabel.setTextColor(getResources().getColor(R.color.black));
        }

    }

    @Override
    public void onRefresh() {
    }

    private void setImageAvatar(Context context, String imgBase64) {
        try {
            Resources res = getResources();
            Bitmap src;
            if (imgBase64.equals("default")) {
                src = BitmapFactory.decodeResource(res, R.drawable.default_avata);
            } else {
                byte[] decodedString = Base64.decode(imgBase64, Base64.DEFAULT);
                src = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            }

            profile_picture.setImageDrawable(ImageUtils.roundedImage(context, src));
        } catch (Exception e) {
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_PROFILE_SETTINGS && resultCode == RESULT_OK) {

            String userName = data.getStringExtra("userName");
            String avata = data.getStringExtra("avata");

            displayName.setText(userName);
            setImageAvatar(getActivity(), avata);
            setIsSettingTabClicked(false);

        }
    }

    public void showDialog() {
        dialogWait = new LovelyProgressDialog(mContext);
        dialogWait.setCancelable(false)
                .setIcon(R.drawable.ic_add_friend)
                .setTitle("Please wait, load profile info")
                .setTopColorRes(R.color.primary)
                .show();
    }

    public void hideDialog() {
        if (dialogWait != null)
            dialogWait.dismiss();
    }
}

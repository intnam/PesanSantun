package thesis.intnam.pesansantun.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import thesis.intnam.pesansantun.R;
import thesis.intnam.pesansantun.Utils.ImageUtils;
import thesis.intnam.pesansantun.data.PesanSantunDB;
import thesis.intnam.pesansantun.data.SharedPreferenceHelper;
import thesis.intnam.pesansantun.data.StaticConfig;
import thesis.intnam.pesansantun.entity.Friend;
import thesis.intnam.pesansantun.entity.Message;

public class ConversationItem extends AbstractItem<ConversationItem, ConversationItem.ViewHolder> {

    public Message message;
    protected Context mContext;

    public ConversationItem(Message message) {
        this.message = message;
    }


    public ConversationItem() {
        this.message = new Message();
    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }


    @Override
    public int getType() {
        return 0;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.conversation_item;
    }

    @Override
    public void bindView(ViewHolder holder, List payloads) {
        super.bindView(holder, payloads);
        mContext = holder.itemView.getContext();

        String msgTime = new SimpleDateFormat("HH:mm").format(new Date(message.timestamp));
        Bitmap avata;
        if (message.idSender.equals(StaticConfig.UID)) {
            holder.user_msg_layout.setVisibility(View.VISIBLE);
            holder.friend_msg_layout.setVisibility(View.GONE);
            holder.userMessage.setText(message.text);
            holder.user_msgStatus.setText(message.status);
            holder.user_msgTime.setText(msgTime);

            String base64AvataUser = SharedPreferenceHelper.getInstance(mContext).getUserInfo().avata;

            if (!base64AvataUser.equals(StaticConfig.STR_DEFAULT_BASE64)) {
                byte[] decodedString = Base64.decode(base64AvataUser, Base64.DEFAULT);
                avata = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            } else {
                avata = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.default_avata);
            }
            holder.userPicture.setImageDrawable(ImageUtils.roundedImage(mContext, avata));
        } else {
            holder.friend_msg_layout.setVisibility(View.VISIBLE);
            holder.user_msg_layout.setVisibility(View.GONE);
            holder.friendMessage.setText(message.text);
            holder.friend_msgTime.setText(msgTime);

            Friend friendData = PesanSantunDB.getInstance(mContext).getFriendById(message.idSender);
            if (friendData.avata == null || friendData.avata.equals("default")) {
                avata = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.default_avata);
            } else {
                byte[] decodedString = Base64.decode(friendData.avata, Base64.DEFAULT);
                avata = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            }

            holder.friendPicture.setImageDrawable(ImageUtils.roundedImage(mContext, avata));
        }


        if (message.isDisplayedTime) {
            holder.chat_date_layout.setVisibility(View.VISIBLE);
            holder.chat_date.setText(new SimpleDateFormat("dd MMM yyyy").format(new Date(message.timestamp)));
        } else {
            holder.chat_date_layout.setVisibility(View.GONE);
        }


    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

        @BindView(R.id.chat_date_layout)
        protected LinearLayout chat_date_layout;

        @BindView(R.id.user_msg_layout)
        protected LinearLayout user_msg_layout;

        @BindView(R.id.friend_msg_layout)
        protected LinearLayout friend_msg_layout;

        @BindView(R.id.chat_date)
        protected TextView chat_date;

        @BindView(R.id.userMessage)
        protected TextView userMessage;

        @BindView(R.id.userPicture)
        protected CircleImageView userPicture;

        @BindView(R.id.user_msgTime)
        protected TextView user_msgTime;

        @BindView(R.id.user_msgStatus)
        protected TextView user_msgStatus;

        @BindView(R.id.friendMessage)
        protected TextView friendMessage;

        @BindView(R.id.friendPicture)
        protected CircleImageView friendPicture;

        @BindView(R.id.friend_msgTime)
        protected TextView friend_msgTime;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;


        }
    }
}
